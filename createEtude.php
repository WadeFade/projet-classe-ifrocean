<?php
require "headerAll.php";
session_start();
mon_header("Créer une étude");

if ($_SESSION["estAdmin"] != 1) {
    echo "<h2>Vous n'avez pas accès à cette page !</h2>";
    require_once "footer.php";
    mon_footer();
    die();
}

$id_Compte = $_SESSION["id_Compte"];
?>

<h1 class="text-center">Créer une étude</h1>
<form action="actions/actionCreateEtude.php" method="post">
    <div class="form-group">
        <label for="titre">Titre</label>
        <input type="text" class="form-control" id="titre" name="titre" placeholder="Titre..." maxlength="50" required>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" id="description" name="description" placeholder="Description...">
    </div>
    <div class="form-group">
        <label for="dateDeb">Date de Début</label>
        <input type="date" class="form-control" id="dateDeb" name="dateDeb" placeholder="Date..." required>
    </div>
    <input type="hidden" id="id_Compte" name="id_Compte" value="<?php echo $id_Compte ?>">
    <a href="pageAdmin.php" class="btn btn-primary pull-left">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>
    <button type="submit" class="btn btn-primary"><i class="fal fa-check"></i> Créer</button>
</form>

<?php require "footer.php";
mon_footer();
?>
