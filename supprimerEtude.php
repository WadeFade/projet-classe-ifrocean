<?php require "headerAll.php";
session_start();
mon_header('Supprimer une Étude');

if ($_SESSION["estAdmin"] != 1) {
    echo "<h2>Vous n'avez pas accès à cette page !</h2>";
    require_once "footer.php";
    mon_footer();
    die();
}

$id = filter_input(INPUT_GET, "etude");

require "config.php";
$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete=$db->prepare("select titre, description from `etude` where id_Etude=:id");
$requete->bindParam(":id", $id);
$requete->execute();

$lignes=$requete->fetchAll();

if (count($lignes)!=1){
    //renvoyer une erreur 404
    echo "Cet id n'existe pas...";
    http_response_code(404);
    include 'footer.php';
    mon_footer();
    die();
}

$titre = $lignes[0]['titre'];

?>

<h1 class="text-center">Supprimer une étude</h1>
<form method="post" action="actions/actionSuppressionEtude.php">
    <h2 class="text-center">Êtes-vous sur de vouloir supprimer l'étude : <?php echo "<i> $titre</i>" ?></h2>
    <h2 class="text-center">avec tout ce qui est liées ?</h2>
    <input type="hidden" name="id_Etude" value="<?php echo $id ?>">

    <a href="pageAdmin.php" class="btn btn-primary pull-left">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>
    <button type="submit" class="btn btn-danger">Confirmer</button>
</form>

<?php require "footer.php";
mon_footer();
?>
