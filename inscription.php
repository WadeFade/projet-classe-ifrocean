<?php require "header.php";
mon_header("Page d'inscription");
$erreur = filter_input(INPUT_GET, "erreur");
?>
<h1 class="titleCenter">Page d'inscription</h1>
<?php if ($erreur==1){
    echo "<div class=\"alert alert-danger formConnection\" role=\"alert\">Veuillez réessayer</div>";
} elseif ($erreur==2){
    echo "<div class=\"alert alert-danger formConnection\" role=\"alert\">Cette adresse email est déjà utilisé</div>";
}
?>
<div class="formConnection">"*" = obligatoire</div>
<form method="post" action="actions/actionInscription.php">
    <div class="form-group formConnection">
        <label for="nom">Nom</label>
        <input type="text" name="nom" class="form-control" id="nom" placeholder="Votre nom...">
    </div>
    <div class="form-group formConnection">
        <label for="prenom">Prénom</label>
        <input type="text" name="prenom" class="form-control" id="prenom" placeholder="Votre prénom...">
    </div>
    <div class="form-group formConnection">
        <label for="username">Nom d'utilisateur*</label>
        <input type="text" name="username" class="form-control" id="username" placeholder="Votre username..." required>
    </div>
    <div class="form-group formConnection">
        <label for="email">Adresse mail*</label>
        <input type="text" name="email" class="form-control" id="email" aria-describedby="email" placeholder="Votre adresse mail..." required>
        <small id="emailHelp" class="form-text text-muted"></small>
    </div>
    <div class="form-group formConnection">
        <label for="password">Mot de passe*</label>
        <input type="password" name="password" class="form-control" id="password" placeholder="Votre mot de passe..." required>
    </div>
    <div class="form-group formConnection">
        <label for="confirm">Confirmez votre mot de passe*</label>
        <input type="password" name="confirm" class="form-control" id="confirm" placeholder="Confirmez votre mot de passe..." required>
    </div>

    <button type="submit" name="submit" class="btn btn-primary formCenter">Créer un compte</button>
</form>


<?php require "footer.php";
mon_footer();
?>