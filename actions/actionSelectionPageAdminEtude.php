<?php
// recupe le nom uniquement avec le post -> select espece- -> recup id suivant nom -> select avant
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$EtudeSelectionner = filter_input(INPUT_POST, "EtudeSelectionner");
$id_Plage = filter_input(INPUT_POST,"id_Plage");

$request = $bd->prepare("select id_Plage,id_Etude
from `etude_plage` where id_Etude=:id_Etude and id_Plage=:id_Plage");
$request->bindParam(":id_Plage",$id_Plage);
$request->bindParam(":id_Etude",$EtudeSelectionner);
$request->execute();
$lines = $request->fetchAll();

if($lines[0]["id_Plage"]==null){
    // ajout des éspèces a une zone
$requette = $bd->prepare("insert into `etude_plage`(id_Etude,id_Plage) values (:id_Etude,:id_Plage)");
$requette->bindParam(":id_Etude", $EtudeSelectionner);
$requette->bindParam(":id_Plage",$id_Plage);
$requette->execute();
}

header("location: ../PageAdminEtude.php?EtudeSelection=".$EtudeSelectionner);


