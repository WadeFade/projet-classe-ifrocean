<?php
session_start();

if ($_SESSION["estAdmin"] != 1) {
    echo "<h2>Vous n'avez pas accès à cette page !</h2>";
    die();
}

$PlageSelectionner=filter_input(INPUT_POST,"PlageSelectionner");
$nom = filter_input(INPUT_POST, "nom");
$departement = filter_input(INPUT_POST, "departement");
$commune = filter_input(INPUT_POST, "commune");

require "../config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("insert into `plage` (nom, departement, commune) values (:nom, :departement, :commune)");
$requete->bindParam(":nom", $nom);
$requete->bindParam(":departement", $departement);
$requete->bindParam(":commune", $commune);
$requete->execute();

$requete=$db->prepare("select * from plage order by id_Plage desc");
$requete->execute();
$lines=$requete->fetchAll();
$PlageSelectionner=$lines[0]["id_Plage"];

header("location: ../pageAdminPlage.php?PlageSelection=".$PlageSelectionner);
