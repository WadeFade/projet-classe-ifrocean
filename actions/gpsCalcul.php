<?php
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$ZoneSelectionner = filter_input(INPUT_POST, "ZoneSelectionner");
$ZoneSelectionner=31;
//localisation ->
// Latitude/Longitude Marée haute Position 1
$MareeHauteLatitudePos1 = filter_input(INPUT_POST, "mareeHLat1");
$MareeHauteLongitudePos1 = filter_input(INPUT_POST, "mareeHLon1");
// Latitude/Longitude Marée haute Position 2
$MareeHauteLatitudePos2 = filter_input(INPUT_POST, "mareeHLat2");
$MareeHauteLongitudePos2 = filter_input(INPUT_POST, "mareeHLon2");
// Latitude/Longitude Marée basse Position 1
$MareeBasseLatitudePos1 = filter_input(INPUT_POST, "mareeBLat1");
$MareeBasseLongitudePos1 = filter_input(INPUT_POST, "mareeBLon1");
// Latitude/Longitude Marée basse Position 2
$MareeBasseLatitudePos2 = filter_input(INPUT_POST, "mareeBLat2");
$MareeBasseLongitudePos2 = filter_input(INPUT_POST, "mareeBLon2");

$request = $bd->prepare("select * from zone z where id_Zone=:ZoneSelectionner");
$request->bindParam(":ZoneSelectionner", $ZoneSelectionner);
$request->execute();
$lines=$request->fetchAll();

// Latitude/Longitude Marée haute Position 1
$MareeHauteLatitudePos1=$lines[0]["MareeHauteLatPos1"];
$MareeHauteLongitudePos1=$lines[0]["MareeHauteLonPos1"];
// Latitude/Longitude Marée haute Position 2
$MareeHauteLatitudePos2=$lines[0]["MareeHauteLatPos2"];
$MareeHauteLongitudePos2=$lines[0]["MareeHauteLonPos2"];
// Latitude/Longitude Marée basse Position 1
$MareeBasseLatitudePos1=$lines[0]["MareeBasseLatPos1"];
$MareeBasseLongitudePos1=$lines[0]["MareeBasseLonPos1"];
// Latitude/Longitude Marée basse Position 2
$MareeBasseLatitudePos2=$lines[0]["MareeBasseLatPos2"];
$MareeBasseLongitudePos2=$lines[0]["MareeBasseLonPos2"];



function get_distance_m($lat1, $lng1, $lat2, $lng2)
{
    $earth_radius = 6378137;
    $rlo1 = deg2rad($lng1);
    $rla1 = deg2rad($lat1);
    $rlo2 = deg2rad($lng2);
    $rla2 = deg2rad($lat2);
    $dlo = ($rlo2 - $rlo1) / 2;
    $dla = ($rla2 - $rla1) / 2;
    $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin(
                $dlo));
    $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
    return ($earth_radius * $d);
}


// longueur
$x1distanceAB=get_distance_m($MareeHauteLatitudePos1,$MareeHauteLongitudePos1,$MareeHauteLatitudePos2,$MareeHauteLongitudePos2);
// longueur
$x6distanceCD=get_distance_m($MareeBasseLatitudePos1,$MareeBasseLongitudePos1,$MareeBasseLatitudePos2,$MareeBasseLongitudePos2);

// Largeur
$x2distanceAC=get_distance_m($MareeHauteLatitudePos1,$MareeHauteLongitudePos1,$MareeBasseLatitudePos1,$MareeBasseLongitudePos1);
// Largeur
$x5distanceBD=get_distance_m($MareeHauteLatitudePos2,$MareeHauteLongitudePos2,$MareeBasseLatitudePos2,$MareeBasseLongitudePos2);

// Diagonale
$x3distanceAD=get_distance_m($MareeHauteLatitudePos1,$MareeHauteLongitudePos1,$MareeBasseLatitudePos2,$MareeBasseLongitudePos2);
// Diagonale
$x4distanceBC=get_distance_m($MareeHauteLatitudePos2,$MareeHauteLongitudePos2,$MareeBasseLatitudePos1,$MareeBasseLongitudePos1);

$airezone=$x1distanceAB*$x2distanceAC;
