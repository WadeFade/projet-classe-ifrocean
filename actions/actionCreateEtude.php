<?php
session_start();

if ($_SESSION["estAdmin"] != 1) {
    echo "<h2>Vous n'avez pas accès à cette page !</h2>";
    die();
}

$titre = filter_input(INPUT_POST, "titre");
$description = filter_input(INPUT_POST, "description");
$dateDeb = filter_input(INPUT_POST, "dateDeb");
$id_Compte = filter_input(INPUT_POST, "id_Compte");

require "../config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("insert into `etude` (titre, description, dateDebut, id_Compte) values (:titre, :description, :dateDebut, :id_Compte)");
$requete->bindParam(":titre", $titre);
$requete->bindParam(":description", $description);
$requete->bindParam(":dateDebut", $dateDeb);
$requete->bindParam(":id_Compte", $id_Compte);
$requete->execute();


header("location: ../pageAdmin.php");
?>