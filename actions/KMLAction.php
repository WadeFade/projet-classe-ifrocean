<?php
header('Content-Type: application/vnd.google-earth.kml+xml kml');
header('Content-Disposition: attachment; filename="test.kml"');
require "../config.php";

$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$ZoneSelectionner = filter_input(INPUT_POST, "ZoneSelectionner");
$ZoneSelectionner=31;
//localisation ->
// Latitude/Longitude Marée haute Position 1
$MareeHauteLatitudePos1 = filter_input(INPUT_POST, "mareeHLat1");
$MareeHauteLongitudePos1 = filter_input(INPUT_POST, "mareeHLon1");
// Latitude/Longitude Marée haute Position 2
$MareeHauteLatitudePos2 = filter_input(INPUT_POST, "mareeHLat2");
$MareeHauteLongitudePos2 = filter_input(INPUT_POST, "mareeHLon2");
// Latitude/Longitude Marée basse Position 1
$MareeBasseLatitudePos1 = filter_input(INPUT_POST, "mareeBLat1");
$MareeBasseLongitudePos1 = filter_input(INPUT_POST, "mareeBLon1");
// Latitude/Longitude Marée basse Position 2
$MareeBasseLatitudePos2 = filter_input(INPUT_POST, "mareeBLat2");
$MareeBasseLongitudePos2 = filter_input(INPUT_POST, "mareeBLon2");

$result = $bd->prepare("select * from zone z where id_Zone=:ZoneSelectionner");
$result->bindParam(":ZoneSelectionner", $ZoneSelectionner);
$result->execute();
$lines=$result->fetchAll();

// Latitude/Longitude Marée haute Position 1
$MareeHauteLatitudePos1=$lines[0]["MareeHauteLatPos1"];
$MareeHauteLongitudePos1=$lines[0]["MareeHauteLonPos1"];
// Latitude/Longitude Marée haute Position 2
$MareeHauteLatitudePos2=$lines[0]["MareeHauteLatPos2"];
$MareeHauteLongitudePos2=$lines[0]["MareeHauteLonPos2"];
// Latitude/Longitude Marée basse Position 1
$MareeBasseLatitudePos1=$lines[0]["MareeBasseLatPos1"];
$MareeBasseLongitudePos1=$lines[0]["MareeBasseLonPos1"];
// Latitude/Longitude Marée basse Position 2
$MareeBasseLatitudePos2=$lines[0]["MareeBasseLatPos2"];
$MareeBasseLongitudePos2=$lines[0]["MareeBasseLonPos2"];


// Print the head of the document
echo '<?xml version="1.0" encoding="UTF-8"?>';
echo '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">';
echo '<Document>';

// Now iterate over all placemarks (rows)
while ($row = mysql_fetch_array($result)) {

    // This writes out a placemark with some data

    echo '<Placemark>';
    echo '<name>' . $row['libelle'] . '</name>';
    echo '<description>' . $row['description'] . '</description>';
    echo '<Point>';
    echo '<coordinates>' . $row['MareeHauteLatPos1'] . ' , ' . $row['MareeHauteLonPos1'] . '</coordinates>';
    echo '</Point>';
    echo '</Placemark>';


};

// And finish the document

echo '</Document>';
echo '</kml>';


