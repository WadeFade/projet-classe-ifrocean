<?php
// recupe le nom uniquement avec le post -> select espece- -> recup id suivant nom -> select avant
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$idEspece= filter_input(INPUT_GET,"idEspece");
$idZone= filter_input(INPUT_GET,"idZone");

$requette = $bd->prepare("delete from zone_espece where id_Espece=:idEspece and id_Zone=:idZone");
$requette ->bindParam(":idEspece", $idEspece);
$requette ->bindParam(":idZone", $idZone);
$requette->execute();

header("location: ../selectionBenevoleComptage.php?ZoneSelectionner=".$idZone);


