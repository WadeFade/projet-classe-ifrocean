<?php
// recupe le nom uniquement avec le post -> select espece- -> recup id suivant nom -> select avant
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$ZoneSelectionner = filter_input(INPUT_POST, "ZoneSelectionner");
$EspeceSelectionner = filter_input(INPUT_POST,"idEspece");

// ajout des éspèces a une zone
$requette = $bd->prepare("insert into `zone_espece`(id_Espece,id_Zone,nombre) values (:EspeceSelectionner,:ZoneSelectionner,0)");
$requette->bindParam(":EspeceSelectionner", $EspeceSelectionner);
$requette->bindParam(":ZoneSelectionner",$ZoneSelectionner);
$requette->execute();

header("location: ../selectionBenevoleComptage.php?ZoneSelectionner=".$ZoneSelectionner);


