<?php
// recupe le nom uniquement avec le post -> select espece- -> recup id suivant nom -> select avant
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$EtudeSelectionner = filter_input(INPUT_GET, "EtudeSelectionner");
$id_Plage = filter_input(INPUT_GET,"id_Plage");


$requette = $bd->prepare("delete from `etude_plage` where id_Etude=:idEtude and id_Plage=:idPlage");
$requette ->bindParam(":idEtude", $EtudeSelectionner);
$requette ->bindParam(":idPlage", $id_Plage);
$requette->execute();

header("location: ../PageAdminEtude.php?EtudeSelection=".$EtudeSelectionner);


