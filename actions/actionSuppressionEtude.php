<?php
session_start();

if ($_SESSION["estAdmin"] != 1) {
    die();
}

$id_Etude = filter_input(INPUT_POST, "id_Etude");


require "../config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

//Supprimer l'étude la cascade fait en sorte de supprimer les zones et les assoc etude_plage correspondantes
$requete3 = $db->prepare("delete from `etude` where id_Etude=:id_Etude");
$requete3->bindParam(":id_Etude", $id_Etude);
$requete3->execute();
var_dump($id_Etude);
header("location: ../pageAdmin.php");
?>