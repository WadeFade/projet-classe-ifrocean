<?php
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$PlageSelectionner=filter_input(INPUT_POST,"PlageSelectionner");
$EtudeSelectionner=filter_input(INPUT_POST,"EtudeSelectionner");
$nom=filter_input(INPUT_POST,"nom");
$departement=filter_input(INPUT_POST,"departement");
$commune=filter_input(INPUT_POST,"commune");
$compteur=filter_input(INPUT_POST,"compteur");

for($i=0;$i<$compteur;$i++){
    $superficie[$i]=filter_input(INPUT_POST,"superficie".$i);
    $EtudePlageSelectionner[$i]=filter_input(INPUT_POST,"EtudePlageSelectionner".$i);
}


if (isset($_POST['Save'])||isset($_POST['SaveSuperficie'])) {
    // j'ai cliqué sur un des boutons pour arrivé ici
    $request = null;
    $lines = null;

    // update de l'étude
    $request = $bd->prepare("update `plage` set nom=:nom,
                   departement=:departement,
                   commune=:commune
where id_Plage=:PlageSelectionner");
    $request->bindParam(":nom", $nom);
    $request->bindParam(":departement", $departement);
    $request->bindParam(":commune", $commune);
    $request->bindParam(":PlageSelectionner", $PlageSelectionner);
    $request->execute();
    $request = null;

    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    // ------------------------------  Boutons Save superficie  ------------------------------------
    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
}
if (isset($_POST['SaveSuperficie'])) {

    // j'ai cliqué sur « Clore »
    $request = null;
    $lines = null;

    for($i=0;$i<$compteur;$i++){
        $request = $bd->prepare("update `etude_plage` set PlageSuperficieTotale=:superficie where id_Etude_Plage=:EtudePlageSelectionner");
        $request->bindParam(":EtudePlageSelectionner", $EtudePlageSelectionner[$i]);
        $request->bindParam(":superficie", $superficie[$i]);
        $request->execute();
        $request = null;
    }

}

header("location: ../PageAdminPlage.php?PlageSelection=".$PlageSelectionner);