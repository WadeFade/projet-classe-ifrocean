<?php session_start();

if (!isset($_SESSION["username"])) {
    echo "<h2>Vous devez vous identifier !</h2>";
    die();
}
$idEtudePlage = filter_input(INPUT_POST, 'idEtudePlage');
$libelle = filter_input(INPUT_POST, 'libelle');
$idCompte = $_SESSION['id_Compte'];

require "../config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("insert into `zone` (libelle, id_Etude_Plage, id_Compte) values (:libelle, :id_Etude_Plage, :id_Compte)");
$requete->bindParam(":libelle", $libelle);
$requete->bindParam(":id_Etude_Plage", $idEtudePlage);
$requete->bindParam(":id_Compte", $idCompte);
$requete->execute();

header("location: ../pageBenevole.php");
?>