<?php
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$EtudeSelectionner = filter_input(INPUT_POST, "EtudeSelectionner");
$idCompte = filter_input(INPUT_POST, "id_Compte");
$Titre = filter_input(INPUT_POST, "Titre");
$Description = filter_input(INPUT_POST, "Description");
$DateDebut = filter_input(INPUT_POST, "DateDebut");
$DateFin = filter_input(INPUT_POST, "DateFin");



if (isset($_POST['Save'])||isset($_POST['Clore'])) {
    // j'ai cliqué sur un des boutons pour arrivé ici
    $request = null;
    $lines = null;

    // update de l'étude
    $request = $bd->prepare("update `etude` set 
titre=:Titre,
description=:Description,
dateDebut=:DateDebut,
dateFin=:DateFin,
id_Compte=:id_Compte
where id_Etude=:EtudeSelectionner");
    $request->bindParam(":Titre", $Titre);
    $request->bindParam(":Description", $Description);
    $request->bindParam(":DateDebut", $DateDebut);
    $request->bindParam(":DateFin", $DateFin);
    $request->bindParam(":id_Compte", $idCompte);
    $request->bindParam(":EtudeSelectionner", $EtudeSelectionner);
    $request->execute();
    $request = null;

    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    // -----------------------------------  Boutons clore  -----------------------------------------
    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
}
//if (isset($_POST['Clore'])) {
//
//    // j'ai cliqué sur « Clore »
//    $request = null;
//    $lines = null;
//
//    // update coordonnée gps date debut
//    $request = $bd->prepare("update `etude` set clos=1 where id_Etude=:EtudeSelectionner");
//    $request->bindParam(":EtudeSelectionner", $EtudeSelectionner);
//    $request->execute();
//    $request = null;
//}

header("location: ../PageAdminEtude.php?EtudeSelection=".$EtudeSelectionner);