<?php
// recupe le nom uniquement avec le post -> select espece- -> recup id suivant nom -> select avant
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$ZoneSelectionner = filter_input(INPUT_GET, "ZoneSelectionner");
$id_Etude_Plage = filter_input(INPUT_GET,"id_Etude_Plage");

$request=$bd->prepare("select id_Etude,id_Plage from etude_plage where id_Etude_Plage=:id_Etude_Plage");
$request->bindParam(":id_Etude_Plage",$id_Etude_Plage);
$request->execute();
$lignes = $request->fetchAll();
$EtudeSelectionner=$lignes[0]["id_Etude"];
$PlageSelectionner=$lignes[0]["id_Plage"];

$request = $bd->prepare("delete from `zone` where id_Etude_Plage=:id_Etude_Plage
and id_Zone=:ZoneSelectionner");
$request ->bindParam(":id_Etude_Plage", $id_Etude_Plage);
$request ->bindParam(":ZoneSelectionner", $ZoneSelectionner);
$request->execute();

header("location: ../PageAdminPlage.php?EtudeSelection=".$EtudeSelectionner."&amp;PlageSelection=".$PlageSelectionner);


