<?php
$nom = filter_input(INPUT_POST, 'nom');
$prenom = filter_input(INPUT_POST, 'prenom');
$username = filter_input(INPUT_POST, 'username');
$email = filter_input(INPUT_POST, 'email');
$password = filter_input(INPUT_POST, 'password');
$confirm = filter_input(INPUT_POST, 'confirm');
$estAdmin = 0;

if (isset($_POST['submit'])) {
    if ($password != $confirm) {
        $erreur = 1;
        header("location: ../inscription.php?erreur=$erreur");

    }
$hashPassword = password_hash($password, PASSWORD_DEFAULT);

    require "../config.php";
    $db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
    $requete = $db->prepare("select email from compte");
    $requete->execute();
    $data = $requete->fetchAll();

    foreach ($data as $ligne){
        if (($ligne[0]['email'])==$email){
            $erreur=2;
            header("location: ../inscription.php?erreur=$erreur");
        }
    }

$requete2 = $db->prepare("insert into compte (nom, prenom, username, password, estAdmin, email) values (:nom , :prenom, :username, :password, :estAdmin, :email)");
    $requete2->bindParam(":nom", $nom);
    $requete2->bindParam(":prenom", $prenom);
    $requete2->bindParam(":username", $username);
    $requete2->bindParam(":password", $hashPassword);
    $requete2->bindParam(":estAdmin", $estAdmin);
    $requete2->bindParam(":email", $email);
    $requete2->execute();

    header("location: ../index.php");

}

?>