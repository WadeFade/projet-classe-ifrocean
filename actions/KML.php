<?php
// Creates the Document.
$dom = new DOMDocument('1.0', 'UTF-8');

require "../config.php";

$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$ZoneSelectionner = filter_input(INPUT_POST, "ZoneSelectionner");
$ZoneSelectionner=31;
//localisation ->
// Latitude/Longitude Marée haute Position 1
$MareeHauteLatitudePos1 = filter_input(INPUT_POST, "mareeHLat1");
$MareeHauteLongitudePos1 = filter_input(INPUT_POST, "mareeHLon1");
// Latitude/Longitude Marée haute Position 2
$MareeHauteLatitudePos2 = filter_input(INPUT_POST, "mareeHLat2");
$MareeHauteLongitudePos2 = filter_input(INPUT_POST, "mareeHLon2");
// Latitude/Longitude Marée basse Position 1
$MareeBasseLatitudePos1 = filter_input(INPUT_POST, "mareeBLat1");
$MareeBasseLongitudePos1 = filter_input(INPUT_POST, "mareeBLon1");
// Latitude/Longitude Marée basse Position 2
$MareeBasseLatitudePos2 = filter_input(INPUT_POST, "mareeBLat2");
$MareeBasseLongitudePos2 = filter_input(INPUT_POST, "mareeBLon2");

$request = $bd->prepare("select * from zone z where id_Zone=:ZoneSelectionner");
$request->bindParam(":ZoneSelectionner", $ZoneSelectionner);
$request->execute();
$lines=$request->fetchAll();

// Latitude/Longitude Marée haute Position 1
$MareeHauteLatitudePos1=$lines[0]["MareeHauteLatPos1"];
$MareeHauteLongitudePos1=$lines[0]["MareeHauteLonPos1"];
// Latitude/Longitude Marée haute Position 2
$MareeHauteLatitudePos2=$lines[0]["MareeHauteLatPos2"];
$MareeHauteLongitudePos2=$lines[0]["MareeHauteLonPos2"];
// Latitude/Longitude Marée basse Position 1
$MareeBasseLatitudePos1=$lines[0]["MareeBasseLatPos1"];
$MareeBasseLongitudePos1=$lines[0]["MareeBasseLonPos1"];
// Latitude/Longitude Marée basse Position 2
$MareeBasseLatitudePos2=$lines[0]["MareeBasseLatPos2"];
$MareeBasseLongitudePos2=$lines[0]["MareeBasseLonPos2"];


function get_distance_m($lat1, $lng1, $lat2, $lng2)
{
    $earth_radius = 6378137;
    $rlo1 = deg2rad($lng1);
    $rla1 = deg2rad($lat1);
    $rlo2 = deg2rad($lng2);
    $rla2 = deg2rad($lat2);
    $dlo = ($rlo2 - $rlo1) / 2;
    $dla = ($rla2 - $rla1) / 2;
    $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin(
                $dlo));
    $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
    return ($earth_radius * $d);
}

// longueur
$x1distanceAB=get_distance_m($MareeHauteLatitudePos1,$MareeHauteLongitudePos1,$MareeHauteLatitudePos2,$MareeHauteLongitudePos2);
// longueur
$x6distanceCD=get_distance_m($MareeBasseLatitudePos1,$MareeBasseLongitudePos1,$MareeBasseLatitudePos2,$MareeBasseLongitudePos2);

// Largeur
$x2distanceAC=get_distance_m($MareeHauteLatitudePos1,$MareeHauteLongitudePos1,$MareeBasseLatitudePos1,$MareeBasseLongitudePos1);
// Largeur
$x5distanceBD=get_distance_m($MareeHauteLatitudePos2,$MareeHauteLongitudePos2,$MareeBasseLatitudePos2,$MareeBasseLongitudePos2);

// Diagonale
$x3distanceAD=get_distance_m($MareeHauteLatitudePos1,$MareeHauteLongitudePos1,$MareeBasseLatitudePos2,$MareeBasseLongitudePos2);
// Diagonale
$x4distanceBC=get_distance_m($MareeHauteLatitudePos2,$MareeHauteLongitudePos2,$MareeBasseLatitudePos1,$MareeBasseLongitudePos1);


$airezone=$x1distanceAB*$x2distanceAC;

// Selects all the rows in the markers table.
$query=$bd->prepare("SELECT * FROM zone z WHERE z.id_Zone=:ZoneSelectionner");
$query->bindParam(":ZoneSelectionner",$ZoneSelectionner);
$query->execute();
$result=$query->fetchAll();


// Creates the root KML element and appends it to the root document.
$node = $dom->createElementNS('http://earth.google.com/kml/2.1', 'kml');
$parNode = $dom->appendChild($node);

// Creates a KML Document element and append it to the KML element.
$dnode = $dom->createElement('Document');
$docNode = $parNode->appendChild($dnode);

// Creates the style elements A
$restStyleNode = $dom->createElement('Style');
$restStyleNode->setAttribute('id', 'positionAstyle');
$restIconstyleNode = $dom->createElement('IconStyle');
$restIconstyleNode->setAttribute('id', 'paddleAicon');
$restIconNode = $dom->createElement('Icon');
$restHref = $dom->createElement('href', 'http://maps.google.com/mapfiles/kml/paddle/A.png');
$restIconNode->appendChild($restHref);
$restIconstyleNode->appendChild($restIconNode);
$restStyleNode->appendChild($restIconstyleNode);
$docNode->appendChild($restStyleNode);

// Creates the style elements B
$restStyleNode = $dom->createElement('Style');
$restStyleNode->setAttribute('id', 'positionBstyle');
$restIconstyleNode = $dom->createElement('IconStyle');
$restIconstyleNode->setAttribute('id', 'paddleBicon');
$restIconNode = $dom->createElement('Icon');
$restHref = $dom->createElement('href', 'http://maps.google.com/mapfiles/kml/paddle/B.png');
$restIconNode->appendChild($restHref);
$restIconstyleNode->appendChild($restIconNode);
$restStyleNode->appendChild($restIconstyleNode);
$docNode->appendChild($restStyleNode);

// Creates the style elements C
$restStyleNode = $dom->createElement('Style');
$restStyleNode->setAttribute('id', 'positionCstyle');
$restIconstyleNode = $dom->createElement('IconStyle');
$restIconstyleNode->setAttribute('id', 'paddleCicon');
$restIconNode = $dom->createElement('Icon');
$restHref = $dom->createElement('href', 'http://maps.google.com/mapfiles/kml/paddle/C.png');
$restIconNode->appendChild($restHref);
$restIconstyleNode->appendChild($restIconNode);
$restStyleNode->appendChild($restIconstyleNode);
$docNode->appendChild($restStyleNode);

// Creates the style elements D
$restStyleNode = $dom->createElement('Style');
$restStyleNode->setAttribute('id', 'positionDstyle');
$restIconstyleNode = $dom->createElement('IconStyle');
$restIconstyleNode->setAttribute('id', 'paddleDicon');
$restIconNode = $dom->createElement('Icon');
$restHref = $dom->createElement('href', 'http://maps.google.com/mapfiles/kml/paddle/D.png');
$restIconNode->appendChild($restHref);
$restIconstyleNode->appendChild($restIconNode);
$restStyleNode->appendChild($restIconstyleNode);
$docNode->appendChild($restStyleNode);

// Iterates through the MySQL results, creating one Placemark for each row.
foreach ($result as $row){
//    ******************************************************************************************************************
    // ************************************************* POINT A *******************************************************
    //******************************************************************************************************************
    // Creates a Placemark and append it to the Document.
    $node = $dom->createElement('PlacemarkA');
    $placeNode = $docNode->appendChild($node);

    // Creates an id attribute and assign it the value of id column.
    $placeNode->setAttribute('id', 'placemarkA' . $row['id_Zone']);

    // Create name, and description elements and assigns them the values of the name and address columns from the results.
    $nameNode = $dom->createElement('name', "point A - ".htmlentities($row['libelle']));
    $placeNode->appendChild($nameNode);
    $descNode = $dom->createElement('description', "Point A");
    $placeNode->appendChild($descNode);
    $styleUrl = $dom->createElement('styleUrl', 'bar');
    $placeNode->appendChild($styleUrl);

    // Creates a Point element.
    $pointNode = $dom->createElement('Point');
    $placeNode->appendChild($pointNode);

    // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
    $coorStr = $row['MareeHauteLatPos1'] . ',' . $row['MareeHauteLonPos1'];
    $coorNode = $dom->createElement('coordinates', $coorStr);
    $pointNode->appendChild($coorNode);
//    ******************************************************************************************************************
    // ************************************************* POINT B *******************************************************
    //******************************************************************************************************************
    // Creates a Placemark and append it to the Document.
    $node = $dom->createElement('PlacemarkB');
    $placeNode = $docNode->appendChild($node);

    // Creates an id attribute and assign it the value of id column.
    $placeNode->setAttribute('id', 'placemarkB' . $row['id_Zone']);

    // Create name, and description elements and assigns them the values of the name and address columns from the results.
    $nameNode = $dom->createElement('name', "point B - ".htmlentities($row['libelle']));
    $placeNode->appendChild($nameNode);
    $descNode = $dom->createElement('description', "Point B");
    $placeNode->appendChild($descNode);
    $styleUrl = $dom->createElement('styleUrl', 'bar');
    $placeNode->appendChild($styleUrl);

    // Creates a Point element.
    $pointNode = $dom->createElement('Point');
    $placeNode->appendChild($pointNode);

    // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
    $coorStr = $row['MareeHauteLatPos2'] . ',' . $row['MareeHauteLonPos2'];
    $coorNode = $dom->createElement('coordinates', $coorStr);
    $pointNode->appendChild($coorNode);
//    ******************************************************************************************************************
    // ************************************************* POINT C *******************************************************
    //******************************************************************************************************************
    // Creates a Placemark and append it to the Document.
    $node = $dom->createElement('PlacemarkC');
    $placeNode = $docNode->appendChild($node);

    // Creates an id attribute and assign it the value of id column.
    $placeNode->setAttribute('id', 'placemarkC' . $row['id_Zone']);

    // Create name, and description elements and assigns them the values of the name and address columns from the results.
    $nameNode = $dom->createElement('name', "point C - ".htmlentities($row['libelle']));
    $placeNode->appendChild($nameNode);
    $descNode = $dom->createElement('description', "Point C");
    $placeNode->appendChild($descNode);
    $styleUrl = $dom->createElement('styleUrl', 'bar');
    $placeNode->appendChild($styleUrl);

    // Creates a Point element.
    $pointNode = $dom->createElement('Point');
    $placeNode->appendChild($pointNode);

    // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
    $coorStr = $row['MareeBasseLatPos1'] . ',' . $row['MareeBasseLonPos1'];
    $coorNode = $dom->createElement('coordinates', $coorStr);
    $pointNode->appendChild($coorNode);
//    ******************************************************************************************************************
    // ************************************************* POINT D *******************************************************
    //******************************************************************************************************************
    // Creates a Placemark and append it to the Document.
    $node = $dom->createElement('PlacemarkD');
    $placeNode = $docNode->appendChild($node);

    // Creates an id attribute and assign it the value of id column.
    $placeNode->setAttribute('id', 'placemarkD' . $row['id_Zone']);

    // Create name, and description elements and assigns them the values of the name and address columns from the results.
    $nameNode = $dom->createElement('name', "point D - ".htmlentities($row['libelle']));
    $placeNode->appendChild($nameNode);
    $descNode = $dom->createElement('description', "Point D");
    $placeNode->appendChild($descNode);
    $styleUrl = $dom->createElement('styleUrl', 'bar');
    $placeNode->appendChild($styleUrl);

    // Creates a Point element.
    $pointNode = $dom->createElement('Point');
    $placeNode->appendChild($pointNode);

    // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
    $coorStr = $row['MareeBasseLatPos2'] . ',' . $row['MareeBasseLonPos2'];
    $coorNode = $dom->createElement('coordinates', $coorStr);
    $pointNode->appendChild($coorNode);
}

$kmlOutput = $dom->saveXML();
header('Content-type: application/vnd.google-earth.kml+xml');
echo $kmlOutput;
