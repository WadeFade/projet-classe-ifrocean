<!--A MODIFIER QUAND ON AURA LE SERVEUR INSTALLER POUR VOIR S'IL EST POSSIBLE D'INTEGRER LA FONCTION DE CHANGEMENT DE MOT DE PASSE PAR MAIL-->
<?php
session_start();
if (isset($email)){
    $email = filter_input(INPUT_POST, "email");
}
require "../mesFonctions.php";
$new_password = randomPassword(15);


require "../config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $db->prepare("select username, email from COMPTE where email=:email");
$requete->bindParam(":email", $email);
$requete->execute();

$data = $requete->fetchAll();
if ($email!=$data["email"]){
    header("location: ../index.php");
}
elseif ($email==$data["email"]) {
//    $to = $email;
//    $subject = 'Demande de changement de mot de passe';
//    $message = 'Voici votre nouveau mot de passe : '.$new_password;
//    $message = wordwrap($message, 70, "\r\n");
//    $headers = 'From: projet@ifrocean.com' . "\r\n" .
//        'Reply-To: projet@ifrocean.com' . "\r\n" .
//        'X-Mailer: PHP/' . phpversion();
//
//    mail($to, $subject, $message, $headers);


    $password = password_hash($new_password, PASSWORD_DEFAULT);

    $requete = $db->prepare("update CLIENT set password=:password where email=:email");
    $requete->bindParam(":password", $password);
    $requete->bindParam(":email", $email);
    $requete->execute();

    header("location: ../index.php");
}


?>

