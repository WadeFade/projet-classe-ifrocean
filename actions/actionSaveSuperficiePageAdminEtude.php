<?php
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$EtudeSelectionner = filter_input(INPUT_POST, "EtudeSelectionner");
$compteur=filter_input(INPUT_POST,"compteur");
for ($i=0;$i<$compteur;$i++){
    $etudePlage[$i]=filter_input(INPUT_POST,"etudePlage".$i);
    $Superficie[$i]=filter_input(INPUT_POST,"Superficie".$i);
}


if (isset($_POST['SavePlage'])) {
    // j'ai cliqué sur un des boutons pour arrivé ici
    $request = null;
    $lines = null;

    // update de l'étude
    for ($i=0;$i<$compteur;$i++){
        $request = $bd->prepare("update `etude_plage` set 
                         PlageSuperficieTotale=:Superficie
                        where id_Etude_Plage=:etudePlage");
        $request->bindParam(":etudePlage",$etudePlage[$i]);
        $request->bindParam(":Superficie",$Superficie[$i]);
        $request->execute();
        $request = null;
    }
}

header("location: ../PageAdminEtude.php?EtudeSelection=".$EtudeSelectionner);