<?php
session_start();

if ($_SESSION["estAdmin"] != 1) {
    die();
}

$id = filter_input(INPUT_POST, "id");

require "../config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

//Il faut commencé par supprimer les assocs plage/zone avant sinon on ne peut pas supprimer etude/plage
$requete = $db->prepare("delete from zone where id_Etude_Plage=:id");
$requete->bindParam(":id", $id);
$requete->execute();

$requete2 = $db->prepare("delete from etude_plage where id_Etude_Plage=:id");
$requete2->bindParam(":id", $id);
$requete2->execute();

var_dump($id);
header("location: ../pageAdmin.php");


?>