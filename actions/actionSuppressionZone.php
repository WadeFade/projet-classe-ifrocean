<?php
session_start();

if ($_SESSION["estAdmin"] != 1) {
    die();
}

$id = filter_input(INPUT_GET, "id_Zone");
if (!$id) {
    $id = filter_input(INPUT_POST, "id");
}
require "../config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $db->prepare("delete from zone where id_Zone=:id_Zone");
$requete->bindParam(":id_Zone", $id);
$requete->execute();

header("location: ../pageAdmin.php");