<?php
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$ZoneSelectionner = filter_input(INPUT_GET, "ZoneSelectionner");

$request = $bd->prepare("select * from zone z where id_Zone=:ZoneSelectionner");
$request->bindParam(":ZoneSelectionner", $ZoneSelectionner);
$request->execute();
$lines=$request->fetchAll();

// Latitude/Longitude Marée haute Position 1
$MareeHauteLatitudePos1=$lines[0]["MareeHauteLatPos1"];
$MareeHauteLongitudePos1=$lines[0]["MareeHauteLonPos1"];
// Latitude/Longitude Marée haute Position 2
$MareeHauteLatitudePos2=$lines[0]["MareeHauteLatPos2"];
$MareeHauteLongitudePos2=$lines[0]["MareeHauteLonPos2"];
// Latitude/Longitude Marée basse Position 1
$MareeBasseLatitudePos1=$lines[0]["MareeBasseLatPos1"];
$MareeBasseLongitudePos1=$lines[0]["MareeBasseLonPos1"];
// Latitude/Longitude Marée basse Position 2
$MareeBasseLatitudePos2=$lines[0]["MareeBasseLatPos2"];
$MareeBasseLongitudePos2=$lines[0]["MareeBasseLonPos2"];


header("Location:https://maps.googleapis.com/maps/api/staticmap?center=" . $MareeHauteLatitudePos1 . "," . $MareeHauteLongitudePos1 . "&size=1200x1000&maptype=roadmap&markers=color:blue%7Clabel:A%7C" . $MareeHauteLatitudePos1 . "," . $MareeHauteLongitudePos1 . "&markers=color:green%7Clabel:B%7C" . $MareeHauteLatitudePos2 . "," . $MareeHauteLongitudePos2 . "&markers=color:red%7Clabel:C%7C" . $MareeBasseLatitudePos1 . "," . $MareeBasseLongitudePos1 . "&markers=color:red%7Clabel:D%7C" . $MareeBasseLatitudePos2 . "," . $MareeBasseLongitudePos2 . "&zoom=17&key=AIzaSyD9oFdtfNI3g4dEwNAaB93zKTT5mSKZWm4");

