<?php
session_start();
//Vérifier si le submit à bien été pris en compte (mis juste pour dev)
$tokenEnvoye = filter_input(INPUT_POST, "token");
if ($tokenEnvoye != $_SESSION["token"]) {
    echo "Vilain pirate";
    die();
}
if (isset($_POST['submit'])) {
    $email = filter_input(INPUT_POST, 'email');
    $password = filter_input(INPUT_POST, 'password');

    require "../config.php";
    $db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

    $requete = $db->prepare("select password, username, id_Compte, nom, prenom, estAdmin, email from COMPTE where email=:email");
    $requete->bindParam(":email", $email);
    $requete->execute();
}
if ($requete->rowCount() > 0) {
    $data = $requete->fetchAll();
    $erreur = 0;

    if (password_verify($password, $data[0]["password"])) {
        $erreur = 0;
        $_SESSION["id_Compte"] = $data[0]["id_Compte"];
        $_SESSION["username"] = $data[0]["username"];
        $_SESSION["email"] = $email;
        $_SESSION["estAdmin"] = $data[0]["estAdmin"];
        if ($_SESSION["estAdmin"]==1){
            header("location: ../pageAdmin.php");
        }
        elseif ($_SESSION["estAdmin"]==0){
            header("location: ../pageBenevole.php");
        }
// à poursuivre quand on aura les autres pages
    } else {
        $erreur = 1;
        header("location: ../index.php?erreur=$erreur");
    }
} else {
    $erreur = 1;
    header("location: ../index.php?erreur=$erreur");
}


?>












