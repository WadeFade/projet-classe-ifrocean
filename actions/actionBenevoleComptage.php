<?php
require "../config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$ZoneSelectionner = filter_input(INPUT_POST, "ZoneSelectionner");
$libelle = filter_input(INPUT_POST, "libelle");

//valeur especes comptés ->
$compteur = filter_input(INPUT_POST, "compteur");
for ($i = 0; $i < $compteur; $i++) {
    $id_Espece[$i] = filter_input(INPUT_POST, "id_Espece" . $i);
    $nom[$i] = filter_input(INPUT_POST, "nom" . $i);
    $nombre[$i] = filter_input(INPUT_POST, "nombre" . $i);
}

//localisation ->
// Latitude/Longitude Marée haute Position 1
$MareeHauteLatitudePos1 = filter_input(INPUT_POST, "mareeHLat1");
$MareeHauteLongitudePos1 = filter_input(INPUT_POST, "mareeHLon1");
// Latitude/Longitude Marée haute Position 2
$MareeHauteLatitudePos2 = filter_input(INPUT_POST, "mareeHLat2");
$MareeHauteLongitudePos2 = filter_input(INPUT_POST, "mareeHLon2");
// Latitude/Longitude Marée basse Position 1
$MareeBasseLatitudePos1 = filter_input(INPUT_POST, "mareeBLat1");
$MareeBasseLongitudePos1 = filter_input(INPUT_POST, "mareeBLon1");
// Latitude/Longitude Marée basse Position 2
$MareeBasseLatitudePos2 = filter_input(INPUT_POST, "mareeBLat2");
$MareeBasseLongitudePos2 = filter_input(INPUT_POST, "mareeBLon2");

$estClos = 0;
var_dump($ZoneSelectionner);
if(isset($_POST['decloturer'])) {
    $request = $bd->prepare("update `zone` set 
estClos=:clos
where id_Zone=:id_Zone");
    $request->bindParam(":clos", $estClos);
    $request->bindParam(":id_Zone", $ZoneSelectionner);
    $request->execute();
    $request = null;

}

$estClos = 1;
if (isset($_POST['Save'])||isset($_POST['Clore'])) {
    // j'ai cliqué sur « Save »
    $request = null;
    $lines = null;

    // test si existe ou pas -
    $request = $bd->prepare("select ze.id_Zone,ze.id_Espece,ze.nombre from `espece` e 
join `zone_espece` ze on e.id_Espece=ze.id_Espece
join `zone` z on ze.id_Zone=z.id_Zone
join `etude_plage` ep on z.id_Etude_Plage=ep.id_Etude_Plage
where  z.id_Zone=:ZoneSelectionner");

    $request->bindParam(":ZoneSelectionner", $ZoneSelectionner);
    $request->execute();
    $lines = $request->fetchAll();
    $request = null;

    // update coordonnée gps date debut
    $request = $bd->prepare("update `zone` set 
libelle=:libelle,
MareeHauteLatPos1=:MareeHauteLatitudePos1,
MareeHauteLonPos1=:MareeHauteLongitudePos1,
MareeHauteLatPos2=:MareeHauteLatitudePos2,
MareeHauteLonPos2=:MareeHauteLongitudePos2,
MareeBasseLatPos1=:MareeBasseLatitudePos1,
MareeBasseLonPos1=:MareeBasseLongitudePos1,
MareeBasseLatPos2=:MareeBasseLatitudePos2,
MareeBasseLonPos2=:MareeBasseLongitudePos2
where id_Zone=:id_Zone");
    $request->bindParam(":MareeHauteLatitudePos1", $MareeHauteLatitudePos1);
    $request->bindParam(":MareeHauteLongitudePos1", $MareeHauteLongitudePos1);
    $request->bindParam(":MareeHauteLatitudePos2", $MareeHauteLatitudePos2);
    $request->bindParam(":MareeHauteLongitudePos2", $MareeHauteLongitudePos2);
    $request->bindParam(":MareeBasseLatitudePos1", $MareeBasseLatitudePos1);
    $request->bindParam(":MareeBasseLongitudePos1", $MareeBasseLongitudePos1);
    $request->bindParam(":MareeBasseLatitudePos2", $MareeBasseLatitudePos2);
    $request->bindParam(":MareeBasseLongitudePos2", $MareeBasseLongitudePos2);
    $request->bindParam(":libelle", $libelle);
    $request->bindParam(":id_Zone", $ZoneSelectionner);
    $request->execute();
    $request = null;

    for ($i = 0; $i <= $compteur; $i++) {
        $request = $bd->prepare("update `zone_espece` set 
nombre=:nombre
where id_Espece=:id_Espece AND id_Zone=:id_Zone");
        $request->bindParam(":id_Espece", $id_Espece[$i]);
        $request->bindParam(":id_Zone", $ZoneSelectionner);
        $request->bindParam(":nombre", $nombre[$i]);
        $request->execute();
        $request = null;

    }
    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    // -----------------------------------  Boutons clore  -----------------------------------------
    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
}
if (isset($_POST['Clore'])) {

// update clos en +
    $request = $bd->prepare("update `zone` set 
estClos=:clos
where id_Zone=:id_Zone");
    $request->bindParam(":clos", $estClos);
    $request->bindParam(":id_Zone", $ZoneSelectionner);
    $request->execute();
    $request = null;
}

header("location: ../benevoleComptage.php?PZ=".$ZoneSelectionner);