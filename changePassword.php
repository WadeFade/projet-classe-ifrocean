<!--A MODIFIER QUAND ON AURA LE SERVEUR INSTALLER POUR VOIR S'IL EST POSSIBLE D'INTEGRER LA FONCTION DE CHANGEMENT DE MOT DE PASSE PAR MAIL-->
<?php
session_start();
include "header.php";
mon_header("Réinitialiser le mot de passe");

?>
<h1 class="formConnection">Changement de mot de passe...</h1>
<form action="actions/actionChangePassword.php" method="post">
    <div class="form-group formConnection">
        <label for="email">Adresse email</label>
        <input type="text" name="email" class="form-control" id="email" aria-describedby="email" placeholder="Votre Email...">
        <small id="emailNHelp" class="form-text text-muted"></small>
    </div>
    <button type="submit" name="submit" class="btn btn-primary formCenter">Valider</button>
</form>


<?php
include "footer.php";
mon_footer();
?>
