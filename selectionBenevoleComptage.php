<?php
include "header.php";
mon_header("Benevole Comptage");

$ZoneSelectionner = filter_input(INPUT_GET, "ZoneSelectionner");
if ($ZoneSelectionner == null) {
    $ZoneSelectionner = filter_input(INPUT_POST, "ZoneSelectionner");
}

require "config.php";
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$request = $bd->prepare("select e.id_Espece, e.nom from `espece` e");
$request->execute();
$lines = $request->fetchAll();

?>
<div class="container">
    <form action="actions/actionSelectionBenevoleComptage.php" method="post">
        <div class="form-group">
            <input type="hidden" name="ZoneSelectionner" value="<?php echo $ZoneSelectionner ?>">
            <input type="hidden" id="idEspece" name="idEspece" value="">
            <label for="sel1">Selection des especes :</label>
            <select class="form-control" id="selectEspece" name="selectEspece">
                <option value="" selected>Selectionner une Espece</option>
                <?php
                foreach ($lines as $line) { // début boucle
                    ?>
                    <option value="<?php echo $line["id_Espece"] ?>"><?php echo $line["nom"] ?></option>
                    <?php // fin boucle
                }
                ?>
            </select>
            <div class="float-right my-3">
                <button id="btnSend" disabled type="submit" class="btn btn-primary"><i class="fas fa-plus-square"></i>
                    Ajout espèces
                </button>
            </div>
        </div>
    </form>
    <table class="table bg-secondary">
        </br>
        <div>Especes selectionner de la zone [<?php echo $ZoneSelectionner ?>]</div>
        <tr>
            <th class="text-light">Id</th>
            <th class="text-light">Nom</th>
        </tr>
        <?php
        $request = $bd->prepare("select ze.id_Zone,ze.id_Espece,e.nom from `espece` e 
        join `zone_espece` ze on e.id_Espece=ze.id_Espece
        join `zone` z on ze.id_Zone=z.id_Zone where z.id_Zone=:ZoneSelectionner");
        $request->bindParam(":ZoneSelectionner", $ZoneSelectionner);
        $request->execute();
        $lines = $request->fetchAll();
        foreach ($lines as $line) { // début boucle
            ?>
            <tr>
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" id="id_espece" readonly
                               name="id_espece<?php echo $line["id_Espece"] ?>"
                               value="<?php echo $line["id_Espece"] ?>">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" id="nom_espece" readonly
                               name="nom_espece<?php echo $line["nom"] ?>" value="<?php echo $line["nom"] ?>">
                    </div>
                </td>
                <td>
                    <div class="float-right my-3">
                        <a href="actions/actionSuppressionBenevoleComptage.php?idEspece=<?php echo $line["id_Espece"] ?>&amp;idZone=<?php echo $line["id_Zone"] ?>"
                           class="btn btn-danger">Suppression</a>
                    </div>
                </td>
            </tr>
            <?php // fin boucle
        }
        ?>
    </table>
    <div class="float-right my-3">
        <a href="benevoleComptage.php?PZ=<?php echo $ZoneSelectionner?>" class="btn btn-outline-primary">Retour</a>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#selectEspece').change(function () {
            if ($(this).val() != '') {
                $('#idEspece').val($(this).val());
                $('#btnSend').removeAttr("disabled", "disabled");
            } else {
                $('#idEspece').val('');
                $('#btnSend').attr("disabled", "disabled");
            }
        });
    });
</script>
<?php
include "footer.php";
mon_footer();
?>
