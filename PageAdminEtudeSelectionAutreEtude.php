<?php
session_start();
include "header.php";
mon_header("Page Admin Etude");
require "config.php";

$id_Compte = $_SESSION["id_Compte"];
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$request = $bd->prepare("select * from `etude` e");
$request->bindParam(":EtudeSelectionner", $EtudeSelectionner);
$request->execute();
$lines = $request->fetchAll();

?>


<table class="table bg-secondary">
    <form action="PageAdminEtude.php" method="post">
        <div class="form-group">
            <input type="hidden" name="EtudeSelectionner" value="<?php echo $EtudeSelectionner ?>">
            <input type="hidden" id="idEtude" name="idEtude" value="">
            <label for="sel1">Sélection de l'étude :</label>
            <select class="form-control" id="EtudeSelectionner" name="EtudeSelectionner">
                <option value="" selected>Selectionner une étude</option>
                <?php
                foreach ($lines as $line) { // début boucle
                    ?>
                    <option value="<?php echo $line["id_Etude"] ?>"><?php echo $line["titre"] ?> - <?php echo $line["dateDebut"] ?></option>
                    <?php // fin boucle
                }
                ?>
            </select>
            <div class="float-right my-3">
                <button id="btnSend" disabled type="submit" class="btn btn-primary"><i class="fas fa-plus-square"></i>
                    Changement de l'étude
                </button>
            </div>
        </div>
    </form>
</table>

<div class="d-flex justify-content-end mb-3">
    <a href="PageAdminEtude.php" class="btn btn-outline-primary">Retour</a>
</div>

<script>
    $(document).ready(function () {
        $('#EtudeSelectionner').change(function () {
            if ($(this).val() != '') {
                $('#idEtude').val($(this).val());
                $('#btnSend').removeAttr("disabled", "disabled");
            } else {
                $('#idEtude').val('');
                $('#btnSend').attr("disabled", "disabled");
            }
        });
    });
</script>

<?php
include "footer.php";
mon_footer();
?>
