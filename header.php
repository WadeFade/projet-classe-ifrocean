<?php function mon_header($title){ ?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Site pour administrer les études d'Ifrocean">
    <title><?php echo $title ?></title>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="stylesheets.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/edc8d5fc95.js"></script>
</head>
<body>
<main class="container">
    <nav class="navbar navbar-dark bg-dark">
            <span class="navbar-brand mb-0 h1 col-4">
                <img src="images/ifroceanLogo.png" alt="Logo d'Ifrocean" id="sizeLogo">
            </span>
    </nav>

<?php } ?>