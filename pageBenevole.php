<?php require "headerAll.php";
session_start();
mon_header("Page des bénévoles");

if (!isset($_SESSION["username"])) {
//    echo "<h2>Vous devez vous identifier !</h2>";
//    require_once "footer.php";
//    mon_footer();
//    die();
}

require "config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $db->prepare("select id_Etude, titre, dateDebut from `etude` where dateFin is null or dateFin = '0000-00-00' order by dateDebut");
$requete->execute();
$lignes = $requete->fetchAll();


$requete2 = $db->prepare("select etude_plage.id_Etude,plage.nom, id_Etude_Plage from `plage` join `etude_plage` on plage.id_Plage=etude_plage.id_Plage join `etude` on etude_plage.id_Etude=etude.id_Etude where etude_plage.id_Plage=plage.id_Plage and etude.id_Etude=etude_plage.id_Etude");
$requete2->execute();
$lignes2 = $requete2->fetchAll();


$requete3 = $db->prepare("select id_Zone, libelle, zone.id_Etude_Plage from `zone` join `etude_plage` on zone.id_Etude_Plage=etude_plage.id_Etude_Plage where zone.id_Etude_Plage=etude_plage.id_Etude_Plage order by libelle");
$requete3->execute();
$lignes3 = $requete3->fetchAll();
?>
    <h1 class="text-center">Page d'Accueil (Bénév.)</h1>
<!--Pour créer une zone, implique qu'on à déjà sélectionné l'étude et la plage-->
    <form action="createZone.php" method="post">
        <input type="hidden" value="" id="idEtude" name="idEtude">
        <input type="hidden" value="" id="idEtudePlage" name="idEtudePlage">
        <button type="submit" class="btn btn-primary" id="creerZone" disabled>Créer Zone</button>
    </form>

    <p class="text-center">Sélectionner une zone existante :</p>
    <!--Affichage des études pour pouvoir les sélectionner-->
    <select class="custom-select" id="valueEtude">
        <option value="" selected>Sélectionnez une étude</option>
        <?php foreach ($lignes as $ligne) { ?>
            <option value="<?php echo $ligne['id_Etude'] ?>"><?php echo $ligne['titre'] . " " . $ligne['dateDebut'] ?></option>
        <?php } ?>
    </select>

    <!--Affichage des associations étude_plage pour pouvoir les sélectionner-->
    <select class="custom-select" id="valueEtudePlage" disabled>
        <option value="" id="show1" selected>Sélectionnez une plage</option>
        <?php foreach ($lignes2 as $ligne2) { ?>
            <option value="<?php echo $ligne2['id_Etude_Plage'] ?>" class="e_<?php echo $ligne2['id_Etude'] ?>"><?php echo $ligne2['nom'] ?></option>
        <?php } ?>
    </select>

    <!--Affichage des zones associées à l'association étude_plage pour pouvoir les sélectionner-->
    <select class="custom-select" id="valueZone" disabled>
        <option value="" id="show2" selected>Sélectionnez une zone</option>
        <?php foreach ($lignes3 as $ligne3) { ?>
            <option value="<?php echo $ligne3['id_Zone'] ?>" class="ep_<?php echo $ligne3['id_Etude_Plage'] ?>"><?php echo $ligne3['libelle'] ?></option>
        <?php } ?>
    </select>
    <form action="benevoleComptage.php" method="post">
        <input type="hidden" value="" id="idZone" name="idZone">
        <button type="submit" id="btnSend" class="btn btn-primary" disabled>Modifier</button>
    </form>
    <script>

        $(document).ready(function() {

            $('#valueEtude').change(function () {
                if ($(this).val() != '') {
                    $('#valueEtudePlage').removeAttr('disabled', 'disabled');
                    $("#valueEtudePlage").val("");
                    $('#valueEtudePlage option').show();
                    $("#valueEtudePlage option:not(.e_" + $(this).val() + ")").hide();
                    $("#show1").show();
                    $('#idEtude').val($(this).val());
                } else {
                    $("#valueEtudePlage").attr("disabled", "disabled");
                    $("#valueZone").attr("disabled", "disabled");
                    $('#btnSend').attr("disabled", "disabled");
                    $('#creerZone').attr('disabled', 'disabled');
                }
            });

            $('#valueEtudePlage').change(function () {
                if ($(this).val() != '') {
                    $('#valueZone').removeAttr('disabled', 'disabled');
                    $('#valueZone').val("");
                    $('#valueZone option').show();
                    $("#valueZone option:not(.ep_" + $(this).val() + ")").hide();
                    $('#show2').show();
                    $('#idEtudePlage').val($(this).val());
                    $('#creerZone').removeAttr('disabled', 'disabled');
                } else {
                    $("#valueZone").attr("disabled", "disabled");
                    $('#btnSend').attr("disabled", "disabled");
                    $('#creerZone').attr('disabled', 'disabled');
                }
            });

            $('#valueZone').change(function() {
               if ($(this).val() != '') {
                   $('#idZone').val($(this).val());
                   $('#btnSend').removeAttr("disabled", "disabled");
               } else {
                   $('#idZone').val('');
                   $('#btnSend').attr("disabled", "disabled");
               }

            });
        });

    </script>

<?php require_once "footer.php";
mon_footer();
?>