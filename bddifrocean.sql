-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 20 jan. 2020 à 01:51
-- Version du serveur :  10.4.8-MariaDB
-- Version de PHP :  7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bddifrocean`
--
CREATE DATABASE IF NOT EXISTS `bddifrocean` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `bddifrocean`;

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `id_Compte` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `estAdmin` tinyint(1) NOT NULL DEFAULT 0,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`id_Compte`, `nom`, `prenom`, `username`, `password`, `estAdmin`, `email`) VALUES
(1, 'Gauthier', 'Mathis', 'admin', '$2y$10$F14UmygGO.Ytp2XosMrqC.9pTmAM4XLhswrp97FhJAwltbpc.3AN.', 1, 'math44100@yahoo.fr'),
(3, NULL, NULL, 'benevole1', '$2y$10$f2CpJm7Jwa.pEoXQ4O4EAu84QcbCbt9t5HOYH8/6RKUqX3CYFXNwS', 0, 'ifrocean1@gmail.com'),
(4, 'PremierCompte', 'PremierCompte', 'PremierCompte', 'PremierCompte', 1, 'PremierCompte@PremierCompte.fr'),
(6, 'DeuxiemeCompte', 'DeuxiemeCompte', 'DeuxiemeCompte', 'DeuxiemeCompte', 0, 'DeuxiemeCompte@gmail.com'),
(7, 'TroisiemeCompte', 'TroisiemeCompte', 'TroisiemeCompte', 'TroisiemeCompte', 1, 'TroisiemeCompte@epsi.fr'),
(8, 'Cinquin', 'Andy', 'admin1', '12345', 1, 'cinquin.andy@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `espece`
--

CREATE TABLE `espece` (
  `id_Espece` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `espece`
--

INSERT INTO `espece` (`id_Espece`, `nom`, `description`) VALUES
(1, 'Poro', ''),
(2, 'Père ver', ''),
(3, 'Tiplouf', ''),
(4, 'Patate au beurre', ''),
(5, 'Pikachu', ''),
(6, 'Carapuce', ''),
(7, 'Gargapapa', ''),
(8, 'Onix', ''),
(9, 'Exogorth', '');

-- --------------------------------------------------------

--
-- Structure de la table `etude`
--

CREATE TABLE `etude` (
  `id_Etude` int(11) NOT NULL,
  `titre` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL,
  `id_Compte` int(11) DEFAULT NULL,
  `clos` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etude`
--

INSERT INTO `etude` (`id_Etude`, `titre`, `description`, `dateDebut`, `dateFin`, `id_Compte`, `clos`) VALUES
(17, 'Maladies Xbbb', 'dfdfd', '2020-01-03', '2020-01-26', 1, 1),
(18, 'vcxvcxvcxgfdgdfgdfgfd', 'cxvvcxvcx', '2020-01-11', '2020-01-26', 1, 0),
(150, 'Etude De Test', 'Etude De TestEtude De Test', '2019-12-15', NULL, 1, 0),
(151, 'Etude Bidon', 'Etude BidonEtude BidonEtude Bidon', '2019-12-17', NULL, 1, 0),
(152, 'kljjklljk', 'hjkhkhj', '2020-01-18', '0000-00-00', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `etude_plage`
--

CREATE TABLE `etude_plage` (
  `id_Etude_Plage` int(11) NOT NULL,
  `id_Etude` int(11) NOT NULL,
  `id_Plage` int(11) NOT NULL,
  `PlageSuperficieTotale` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etude_plage`
--

INSERT INTO `etude_plage` (`id_Etude_Plage`, `id_Etude`, `id_Plage`, `PlageSuperficieTotale`) VALUES
(35, 150, 5, 1694),
(36, 151, 5, 192443),
(46, 152, 1, 422442),
(47, 152, 4, 242424),
(48, 150, 1, 9999);

-- --------------------------------------------------------

--
-- Structure de la table `plage`
--

CREATE TABLE `plage` (
  `id_Plage` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `departement` varchar(50) NOT NULL,
  `commune` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `plage`
--

INSERT INTO `plage` (`id_Plage`, `nom`, `departement`, `commune`) VALUES
(1, 'La plage du léman', '74140', 'Sciez'),
(2, 'PremierePlage', '44000', 'Nantes'),
(3, 'DeuxiemePlage', '55555', 'autreVille'),
(4, 'TroisiemePlage', '66666', 'Hell'),
(5, 'La plage du pélican', '7894', 'Hawai');

-- --------------------------------------------------------

--
-- Structure de la table `zone`
--

CREATE TABLE `zone` (
  `id_Zone` int(11) NOT NULL,
  `id_Etude_Plage` int(11) NOT NULL,
  `id_Compte` int(11) NOT NULL,
  `libelle` text DEFAULT NULL,
  `MareeHauteLatPos1` double DEFAULT NULL,
  `MareeHauteLonPos1` double DEFAULT NULL,
  `MareeHauteLatPos2` double DEFAULT NULL,
  `MareeHauteLonPos2` double DEFAULT NULL,
  `MareeBasseLatPos1` double DEFAULT NULL,
  `MareeBasseLonPos1` double DEFAULT NULL,
  `MareeBasseLatPos2` double DEFAULT NULL,
  `MareeBasseLonPos2` double DEFAULT NULL,
  `estClos` tinyint(1) NOT NULL DEFAULT 0,
  `description` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `zone`
--

INSERT INTO `zone` (`id_Zone`, `id_Etude_Plage`, `id_Compte`, `libelle`, `MareeHauteLatPos1`, `MareeHauteLonPos1`, `MareeHauteLatPos2`, `MareeHauteLonPos2`, `MareeBasseLatPos1`, `MareeBasseLonPos1`, `MareeBasseLatPos2`, `MareeBasseLonPos2`, `estClos`, `description`) VALUES
(2, 1, 1, 'Zone 2', 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 1, 0),
(3, 1, 1, 'Zone3', 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 1, 0),
(4, 35, 1, '4Zone', 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 1, 0),
(5, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(7, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(8, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(9, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(10, 1, 1, 'cxw', 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 1, 0),
(11, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(12, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(13, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(14, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(15, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(16, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(17, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(18, 33, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(19, 34, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(20, 33, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(21, 1, 1, NULL, 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(25, 36, 1, 'Zone de testZone de testZone de test', 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(26, 36, 1, 'Zone de testZone de testZone de test', 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 1, 0),
(28, 35, 1, 'Zone de testZone de testZone de test', 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(30, 35, 1, 'Zone de testZone de testZone de test', 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 1, 0),
(31, 35, 1, 'Zone Test - vrai coordonnée', 48.1864361, -4.298263888888889, 48.1863139, -4.298177777777778, 48.18578517188131, -4.301042657428309, 48.1852623941153, -4.300033866416085, 0, 0),
(32, 48, 1, 'Perfect', 47.222, -1.575, 47.224, -1.5755, 47.223, -1.5745, 47.2225, -1.574, 0, 0),
(33, 48, 1, 'ZoneA', 47.2205581, -1.555379, 47.2205545, -1.5745379, 44.2205581, 2.5745379, 3.2205581, -1.5745379, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `zone_espece`
--

CREATE TABLE `zone_espece` (
  `id_Espece` int(11) NOT NULL,
  `id_Zone` int(11) NOT NULL,
  `nombre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `zone_espece`
--

INSERT INTO `zone_espece` (`id_Espece`, `id_Zone`, `nombre`) VALUES
(1, 2, 3),
(1, 4, 4),
(1, 10, 9),
(1, 25, 2147444),
(1, 28, 136),
(1, 31, 222),
(1, 32, 145),
(1, 33, 4),
(2, 2, 9),
(2, 4, 3),
(2, 10, 2),
(2, 31, 333),
(2, 33, 8),
(3, 2, 4),
(3, 4, 1),
(3, 10, 0),
(3, 30, 19),
(3, 31, 444),
(3, 33, 11),
(4, 4, 0),
(5, 4, 3),
(5, 25, 2147483),
(6, 4, 0),
(6, 26, 666666),
(7, 30, 9),
(7, 31, 555),
(7, 32, 69),
(8, 26, 8795),
(8, 30, 36),
(8, 32, 30),
(9, 28, 25),
(9, 31, 666);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`id_Compte`);

--
-- Index pour la table `espece`
--
ALTER TABLE `espece`
  ADD PRIMARY KEY (`id_Espece`);

--
-- Index pour la table `etude`
--
ALTER TABLE `etude`
  ADD PRIMARY KEY (`id_Etude`),
  ADD KEY `ForeignKey_Compte_Etude` (`id_Compte`);

--
-- Index pour la table `etude_plage`
--
ALTER TABLE `etude_plage`
  ADD PRIMARY KEY (`id_Etude_Plage`) USING BTREE,
  ADD KEY `ForeignKey_Plage_Etude_Plage` (`id_Plage`),
  ADD KEY `ForeignKey_Etude_Plage_Etude` (`id_Etude`);

--
-- Index pour la table `plage`
--
ALTER TABLE `plage`
  ADD PRIMARY KEY (`id_Plage`);

--
-- Index pour la table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`id_Zone`),
  ADD KEY `ForeignKey_Compte_Zone` (`id_Compte`),
  ADD KEY `id_Etude_Plage` (`id_Etude_Plage`);

--
-- Index pour la table `zone_espece`
--
ALTER TABLE `zone_espece`
  ADD PRIMARY KEY (`id_Espece`,`id_Zone`),
  ADD KEY `ForeignKey_Zone_Zone_Espece` (`id_Zone`),
  ADD KEY `id_Espece` (`id_Espece`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
  MODIFY `id_Compte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `espece`
--
ALTER TABLE `espece`
  MODIFY `id_Espece` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `etude`
--
ALTER TABLE `etude`
  MODIFY `id_Etude` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT pour la table `etude_plage`
--
ALTER TABLE `etude_plage`
  MODIFY `id_Etude_Plage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT pour la table `plage`
--
ALTER TABLE `plage`
  MODIFY `id_Plage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `zone`
--
ALTER TABLE `zone`
  MODIFY `id_Zone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `etude`
--
ALTER TABLE `etude`
  ADD CONSTRAINT `ForeignKey_Compte_Etude` FOREIGN KEY (`id_Compte`) REFERENCES `compte` (`id_Compte`);

--
-- Contraintes pour la table `etude_plage`
--
ALTER TABLE `etude_plage`
  ADD CONSTRAINT `ForeignKey_Etude_Plage_Etude` FOREIGN KEY (`id_Etude`) REFERENCES `etude` (`id_Etude`) ON DELETE CASCADE,
  ADD CONSTRAINT `ForeignKey_Plage_Etude_Plage` FOREIGN KEY (`id_Plage`) REFERENCES `plage` (`id_Plage`) ON DELETE CASCADE;

--
-- Contraintes pour la table `zone`
--
ALTER TABLE `zone`
  ADD CONSTRAINT `ForeignKey_Compte_Zone` FOREIGN KEY (`id_Compte`) REFERENCES `compte` (`id_Compte`);

--
-- Contraintes pour la table `zone_espece`
--
ALTER TABLE `zone_espece`
  ADD CONSTRAINT `ForeignKey_Espece_Zone_Espece` FOREIGN KEY (`id_Espece`) REFERENCES `espece` (`id_Espece`),
  ADD CONSTRAINT `ForeignKey_Zone_Zone_Espece` FOREIGN KEY (`id_Zone`) REFERENCES `zone` (`id_Zone`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
