<?php require "headerAll.php";
session_start();
mon_header("Création d'une nouvelle zone");

if (!isset($_SESSION["username"])) {
    echo "<h2>Vous devez vous identifier !</h2>";
    require "footer.php";
    mon_footer();
    die();
}

$idEtude = filter_input(INPUT_POST, 'idEtude');
$idEtudePlage = filter_input(INPUT_POST, 'idEtudePlage');

require "config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $db->prepare("select etude_plage.id_Etude, etude.titre, plage.nom from `etude_plage` join `plage` on etude_plage.id_Plage=plage.id_Plage join `etude` on etude_plage.id_Etude=etude.id_Etude where id_Etude_Plage=:id_Etude_Plage");

$requete->bindParam(':id_Etude_Plage', $idEtudePlage);
$requete->execute();
$lignes = $requete->fetchAll();

if (count($lignes)!=1){
    //renvoyer une erreur 404
    echo "Cet id n'existe pas...";
    http_response_code(404);
    include 'footer.php';
    mon_footer();
    die();
}


?>
<h1 class="text-center">Page de création (Zone)</h1>
<p class="text-center">Voulez-vous créer une zone dans cette étude : <?php echo '<b>'.$lignes[0]['titre'].'</b>'; ?> ?</p>
<p class="text-center">Qui est associée à la plage : <?php echo '<b>'.$lignes[0]['nom'].'</b>'; ?> ?</p>

<form action="actions/actionCreateZone.php" method="post">
    <input type="hidden" name="idEtudePlage" value="<?php echo $idEtudePlage ?>">
    <div class="form-group">
        <label for="libelle">Libellé de la zone :</label>
        <input type="text" class="form-control" id="libelle" name="libelle" placeholder="Libellé..." maxlength="20" required>
    </div>
    <a href="pageBenevole.php" class="btn btn-primary pull-left">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>
    <button type="submit" class="btn btn-primary">Créer zone</button>
</form>

<?php require "footer.php";
mon_footer();
?>

