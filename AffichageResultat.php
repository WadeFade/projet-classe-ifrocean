<?php require "headerAll.php";
session_start();
mon_header("Page des résultats");
if (isset($_SESSION["username"])) {
    if ($_SESSION["estAdmin"] != 1) {
        echo "<h2>Vous n'avez pas accès à cette page !</h2>";
        require_once "footer.php";
        mon_footer();
        die();
    }
} else {
    echo "<h2>Vous devez vous identifier !</h2>";
    require_once "footer.php";
    mon_footer();
    die();
}

require "config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $db->prepare("select id_Etude, titre, dateDebut from `etude` where dateFin is null or dateFin = '0000-00-00' order by dateDebut");
$requete->execute();
$lignes = $requete->fetchAll();


function get_distance_m($lat1, $lng1, $lat2, $lng2)
{
    $earth_radius = 6378137;
    $rlo1 = deg2rad($lng1);
    $rla1 = deg2rad($lat1);
    $rlo2 = deg2rad($lng2);
    $rla2 = deg2rad($lat2);
    $dlo = ($rlo2 - $rlo1) / 2;
    $dla = ($rla2 - $rla1) / 2;
    $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin(
                $dlo));
    $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
    return ($earth_radius * $d);
}

?>

    <h1 class="text-center">Page Résultat</h1>

<!--Pour afficher toutes les études en cours-->
<?php foreach ($lignes

               as $ligne) {
$requete2 = $db->prepare("select nom, id_Etude_Plage,dateFin from `plage` join `etude_plage` on plage.id_Plage=etude_plage.id_Plage join `etude` on etude_plage.id_Etude=etude.id_Etude where etude_plage.id_Plage=plage.id_Plage and etude.id_Etude=etude_plage.id_Etude and etude.id_Etude=:id_Etude");
$requete2->bindParam(":id_Etude", $ligne["id_Etude"]);
$requete2->execute();
$lignes2 = $requete2->fetchAll();
?>


<?php //if ($ligne[''])

?>
<table class="table table-striped table-dark">
    <thead>
    <tr>
        <p class="text-center font-weight-bold">Etudes</p>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?php echo $ligne['titre'] ?></td>
        <td><?php echo $ligne['dateDebut'] ?></td>
    </tr>
    <!--    duplication de code, flemme de faire autrement , désolé il est trop tard pour ces bétises :') -->
    <?php
    foreach ($lignes2

             as $ligne2) {
        $requete3 = $db->prepare("select id_Zone, libelle, estClos,MareeBasseLatPos1,MareeBasseLatPos2,MareeBasseLonPos1,MareeBasseLonPos2,MareeHauteLatPos1,MareeHauteLatPos2,MareeHauteLonPos1,MareeHauteLonPos2 from `zone` 
    join `etude_plage` on zone.id_Etude_Plage=etude_plage.id_Etude_Plage 
where zone.id_Etude_Plage=:id_Etude_Plage 
order by libelle");
        $requete3->bindParam(":id_Etude_Plage", $ligne2["id_Etude_Plage"]);
        $requete3->execute();
        $lignes3 = $requete3->fetchAll();

        ?>
<!--        <tr>-->
<!--            <th>Nom espece :</th>-->
<!--            <th>Nombre trouvés :</th>-->
<!--            <th>Nombre au m² :</th>-->
<!--        </tr>-->
        <!--Pour afficher toutes les plages qui sont dans la bonne étude-->
<!--        --><?php //foreach ($lignes2 as $ligne2) {
//            $requete3 = $db->prepare("select id_Zone, libelle, estClos,MareeBasseLatPos1,MareeBasseLatPos2,MareeBasseLonPos1,MareeBasseLonPos2,MareeHauteLatPos1,MareeHauteLatPos2,MareeHauteLonPos1,MareeHauteLonPos2 from `zone`
//    join `etude_plage` on zone.id_Etude_Plage=etude_plage.id_Etude_Plage
//where zone.id_Etude_Plage=:id_Etude_Plage
//order by libelle");
//            $requete3->bindParam(":id_Etude_Plage", $ligne2["id_Etude_Plage"]);
//            $requete3->execute();
//            $lignes3 = $requete3->fetchAll();
//
//            $airezone = 0;
//            foreach ($lignes3 as $ligne3) {
//                $MareeHauteLatitudePos1 = $ligne3["MareeHauteLatPos1"];
//                $MareeHauteLongitudePos1 = $ligne3["MareeHauteLonPos1"];
//                $MareeHauteLatitudePos2 = $ligne3["MareeHauteLatPos2"];
//                $MareeHauteLongitudePos2 = $ligne3["MareeHauteLonPos2"];
//                $MareeBasseLatitudePos1 = $ligne3["MareeBasseLatPos1"];
//                $MareeBasseLongitudePos1 = $ligne3["MareeBasseLonPos1"];
//                $MareeBasseLatitudePos2 = $ligne3["MareeBasseLatPos2"];
//                $MareeBasseLongitudePos2 = $ligne3["MareeBasseLonPos2"];
//                // longueur
//                $x1distanceAB = get_distance_m($MareeHauteLatitudePos1, $MareeHauteLongitudePos1, $MareeHauteLatitudePos2, $MareeHauteLongitudePos2);
//                // longueur
//                $x6distanceCD = get_distance_m($MareeBasseLatitudePos1, $MareeBasseLongitudePos1, $MareeBasseLatitudePos2, $MareeBasseLongitudePos2);
//                // Largeur
//                $x2distanceAC = get_distance_m($MareeHauteLatitudePos1, $MareeHauteLongitudePos1, $MareeBasseLatitudePos1, $MareeBasseLongitudePos1);
//                // Largeur
//                $x5distanceBD = get_distance_m($MareeHauteLatitudePos2, $MareeHauteLongitudePos2, $MareeBasseLatitudePos2, $MareeBasseLongitudePos2);
//                // Diagonale
//                $x3distanceAD = get_distance_m($MareeHauteLatitudePos1, $MareeHauteLongitudePos1, $MareeBasseLatitudePos2, $MareeBasseLongitudePos2);
//                // Diagonale
//                $x4distanceBC = get_distance_m($MareeHauteLatitudePos2, $MareeHauteLongitudePos2, $MareeBasseLatitudePos1, $MareeBasseLongitudePos1);
//
//                $airezone += $x1distanceAB * $x2distanceAC;
//            }
//            $requete4 = $db->prepare("select nom,nombre,PlageSuperficieTotale,SUM(nombre),e.id_Espece from zone z
//    join etude_plage ep on z.id_Etude_Plage = ep.id_Etude_Plage
//    join zone_espece ze on z.id_Zone = ze.id_Zone
//    join espece e on ze.id_Espece = e.id_Espece
//    where z.id_Etude_Plage=:id_Etude_Plage
//    group by id_Espece");
//            $requete4->bindParam(":id_Etude_Plage", $ligne2["id_Etude_Plage"]);
//            $requete4->execute();
//            $lignes4 = $requete4->fetchAll();
//            $nombreTotal=0;
//
//            $requete5 = $db->prepare("select nom from espece");
//            $requete5->bindParam(":id_Etude_Plage", $ligne2["id_Etude_Plage"]);
//            $requete5->execute();
//            $lignes5 = $requete5->fetchAll();
//            foreach ($lignes5 as $ligne5) {
//                $nom=$ligne5["nom"];
//                $nombreTotal=array($nom=>0);
//            }
//
//            foreach ($lignes4 as $ligne4) {
//                $nombremetrecarre = ($ligne4["SUM(nombre)"] / $airezone);
//                $nombreTotal[$ligne4["nom"]]+=$ligne4["SUM(nombre)"];
//            }
//             var_dump($nombreTotal);
//            foreach ($nombreTotal as $value){
//                var_dump($value);
//
//    ?>

<!--    <tr>-->
<!--        <td></td>-->
<!--        <td></td>-->
<!--        <td></td>-->
<!--    </tr>-->

    <tr class="bg-info">
        <td>Plage : <?php echo $ligne2["nom"] ?></td>
    </tr>

    <!-- Calcul de la superficie étudiée  -->
    <?php
    $airezone = 0;
    foreach ($lignes3 as $ligne3) {
        $MareeHauteLatitudePos1 = $ligne3["MareeHauteLatPos1"];
        $MareeHauteLongitudePos1 = $ligne3["MareeHauteLonPos1"];
        $MareeHauteLatitudePos2 = $ligne3["MareeHauteLatPos2"];
        $MareeHauteLongitudePos2 = $ligne3["MareeHauteLonPos2"];
        $MareeBasseLatitudePos1 = $ligne3["MareeBasseLatPos1"];
        $MareeBasseLongitudePos1 = $ligne3["MareeBasseLonPos1"];
        $MareeBasseLatitudePos2 = $ligne3["MareeBasseLatPos2"];
        $MareeBasseLongitudePos2 = $ligne3["MareeBasseLonPos2"];
// longueur
        $x1distanceAB = get_distance_m($MareeHauteLatitudePos1, $MareeHauteLongitudePos1, $MareeHauteLatitudePos2, $MareeHauteLongitudePos2);
//        echo "</br>";
//        var_dump($x1distanceAB);
// longueur
        $x6distanceCD = get_distance_m($MareeBasseLatitudePos1, $MareeBasseLongitudePos1, $MareeBasseLatitudePos2, $MareeBasseLongitudePos2);
//        echo "</br>";
//        var_dump($x6distanceCD);
// Largeur
        $x2distanceAC = get_distance_m($MareeHauteLatitudePos1, $MareeHauteLongitudePos1, $MareeBasseLatitudePos1, $MareeBasseLongitudePos1);
//        echo "</br>";
//        var_dump($x2distanceAC);
// Largeur
        $x5distanceBD = get_distance_m($MareeHauteLatitudePos2, $MareeHauteLongitudePos2, $MareeBasseLatitudePos2, $MareeBasseLongitudePos2);
//        echo "</br>";
//        var_dump($x5distanceBD);
// Diagonale
        $x3distanceAD = get_distance_m($MareeHauteLatitudePos1, $MareeHauteLongitudePos1, $MareeBasseLatitudePos2, $MareeBasseLongitudePos2);
//        echo "</br>";
//        var_dump($x3distanceAD);
// Diagonale
        $x4distanceBC = get_distance_m($MareeHauteLatitudePos2, $MareeHauteLongitudePos2, $MareeBasseLatitudePos1, $MareeBasseLongitudePos1);
//        echo "</br>";
//        var_dump($x4distanceBC);

        $perimetre = $x1distanceAB+$x4distanceBC+$x6distanceCD+$x3distanceAD;
//        todo calcul faux -> regler le pb - idk how -
//        '/100' -> résultat plus jolis / c
        $airezone = $x1distanceAB * $x2distanceAC /100;
//        echo "</br>";
//        var_dump($airezone);
    }
    ?>
    <tr>
        <th>nom :</th>
        <th>nb :</th>
        <th>/m² :</th>
        <th>Nombre au total pour la plage :</th>
    </tr>
    <?php
    $requete4 = $db->prepare("select nom,nombre,PlageSuperficieTotale,SUM(nombre),e.id_Espece from zone z 
    join etude_plage ep on z.id_Etude_Plage = ep.id_Etude_Plage 
    join zone_espece ze on z.id_Zone = ze.id_Zone 
    join espece e on ze.id_Espece = e.id_Espece
    where z.id_Etude_Plage=:id_Etude_Plage
    group by nombre desc");
    $requete4->bindParam(":id_Etude_Plage", $ligne2["id_Etude_Plage"]);
    $requete4->execute();
    $lignes4 = $requete4->fetchAll();
    foreach ($lignes4

    as $ligne4) { ?>
    <tr class="bg-secondary">
        <td><?php echo $ligne4["nom"] ?></td>
        <td><?php echo $ligne4["SUM(nombre)"] ?></td>
        <?php // on a donc un nombre total pour une zone total en m² , si on divise ce nombre par la surface on a donc le nombre par metre
        $nombremetrecarre = ($ligne4["SUM(nombre)"] / $airezone)
        ?>
        <td><?php echo round($nombremetrecarre,5) ?></td>
        <td><?php echo round($nombremetrecarre * $ligne4["PlageSuperficieTotale"],2) ?></td>
    </tr>
    </tbody>
    <?php } ?>
    </tbody>
    <?php }
    } ?>
</table>
<?php
//48.1864361
//-4.298263888888889
//48.1863139
//-4.298177777777778
//48.18578517188131
//-4.301042657428309
//48.1852623941153
//-4.300033866416085
// requetes update violente pour que zone cohérente
//$requete5 = $db->prepare("update `zone` set MareeHauteLatPos1=48.1864361,
//                MareeHauteLonPos1=-4.298263888888889,
//                MareeHauteLatPos2=48.1863139,
//                MareeHauteLonPos2=-4.298177777777778,
//                MareeBasseLatPos1=48.18578517188131,
//                  MareeBasseLonPos1=-4.301042657428309,
//                  MareeBasseLatPos2=48.1852623941153,
//                  MareeBasseLonPos2=-4.300033866416085 where id_Compte=1");
//$requete5->execute();
?>

<?php require_once "footer.php";
mon_footer();
?>


