<?php require "headerAll.php";
session_start();
mon_header("Page des administrateurs");
if (isset($_SESSION["username"])){
    if ($_SESSION["estAdmin"] != 1) {
        echo "<h2>Vous n'avez pas accès à cette page !</h2>";
        require_once "footer.php";
        mon_footer();
        die();
    }
} else {
    echo "<h2>Vous devez vous identifier !</h2>";
    require_once "footer.php";
    mon_footer();
    die();
}

require "config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $db->prepare("select id_Etude, titre, dateDebut from `etude` where dateFin is null or dateFin = '0000-00-00' order by dateDebut");
$requete->execute();
$lignes = $requete->fetchAll();

?>
<h1 class="text-center">Page d'Accueil (Admin)</h1>
<div>
    <a class="btn btn-success" href="createEtude.php" role="button"><i class="fal fa-plus-circle"></i> Etudes</a>
</div>

<div class="my-2"><a href="AffichageResultat.php">Affichage résultat ! </a></div>

<table class="table table-striped table-dark">
    <thead>
    <tr>
        <p class="text-center font-weight-bold">Les études en cours</p>
    </tr>
    </thead>
    <!--Pour afficher toutes les études en cours-->
    <?php foreach ($lignes as $ligne) {
        $requete2 = $db->prepare("select nom, id_Etude_Plage from `plage` join `etude_plage` on plage.id_Plage=etude_plage.id_Plage join `etude` on etude_plage.id_Etude=etude.id_Etude where etude_plage.id_Plage=plage.id_Plage and etude.id_Etude=etude_plage.id_Etude and etude.id_Etude=:id_Etude");
        $requete2->bindParam(":id_Etude", $ligne["id_Etude"]);
        $requete2->execute();
        $lignes2 = $requete2->fetchAll();
        ?>

        <tbody>
        <tr>
            <td><?php echo $ligne['titre'] ?></td>
            <td><?php echo $ligne['dateDebut'] ?></td>
            <td></td>
            <td>
                <a href="PageAdminEtude.php?EtudeSelection=<?php echo $ligne['id_Etude'] ?>" class="btn btn-primary"><i class="fa fa-pen"></i></a>
                <a href="supprimerEtude.php?etude=<?php echo $ligne['id_Etude'] ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            </td>
            <!--Pour afficher toutes les plages qui sont dans la bonne étude-->
            <?php foreach ($lignes2 as $ligne2) {
            $requete3 = $db->prepare("select id_Zone, libelle, estClos from `zone` join `etude_plage` on zone.id_Etude_Plage=etude_plage.id_Etude_Plage where zone.id_Etude_Plage=:id_Etude_Plage order by libelle");
            $requete3->bindParam(":id_Etude_Plage", $ligne2["id_Etude_Plage"]);
            $requete3->execute();
            $lignes3 = $requete3->fetchAll();
            ?>
            <tr class="bg-secondary">
                <td>Plage : <?php echo $ligne2["nom"] ?></td>
                <td></td>
                <td>
                    <!--A voir si on garde la modifier car il correspond à l'association etude-plage et non de la plage en elle même-->
                    <!--Mais on peut quand même aller modifier la plage-->
                    <a href="PageAdminPlage.php?EP=<?php echo $ligne2['id_Etude_Plage'] ?>" class="btn btn-primary"><i class="fa fa-pen"></i></a>
                    <a href="supprimerAssociationEP.php?EP=<?php echo $ligne2['id_Etude_Plage'] ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                </td>
                <!--Pour afficher toutes les zones qui sont dans la bonne plage de la bonne étude-->
                <?php foreach ($lignes3 as $ligne3) {
                    if ($ligne3['estClos']==1){?>
                    <tr class="bg-success">
            <?php }
            else{ ?>
                    <tr class="bg-warning">
            <?php
            }?>
                    <td>Zone : <?php echo $ligne3["libelle"] ?></td>
                    <td>
                        <a href="benevoleComptage.php?PZ=<?php echo $ligne3['id_Zone'] ?>" class="btn btn-primary"><i class="fa fa-pen"></i></a>
                        <a href="supprimerZone.php?PZ=<?php echo $ligne3['id_Zone'] ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php } ?>
            </tr>
        <?php } ?>
        </tr>
        </tbody>
    <?php } ?>
</table>





<?php require_once "footer.php";
mon_footer();
?>
