<?php
session_start();
require "headerAll.php";
mon_header("Page Bénévole comptage");
require "config.php";

if (!isset($_SESSION["username"])) {
    echo "<h2>Vous devez vous identifier !</h2>";
    require_once "footer.php";
    mon_footer();
    die();
}

$ZoneSelectionner = filter_input(INPUT_GET, "PZ");
if ($ZoneSelectionner == NULL) {
    $ZoneSelectionner = filter_input(INPUT_POST, 'idZone');
}
$id_Compte = $_SESSION["id_Compte"];
$estAdmin = $_SESSION['estAdmin'];

$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$request = $bd->prepare("select * from `zone` z  where z.id_Zone=:ZoneSelectionner");
$request->bindParam(":ZoneSelectionner", $ZoneSelectionner);
$request->execute();
$lines = $request->fetchAll();

if (($lines[0]["estClos"] != 0)) {
    if ($estAdmin != 1) {
        echo "<h2>Vous ne pouvez accéder à une zone Cloturée !</h2>
</br>
<p>Référez-vous à un administrateur en cas d'erreur.</p>";
        require "footer.php";
        mon_footer();
        die();
    } else {
        echo "<h2>Zone cloturée</h2>" . "<br>" . "<p>en tant qu'administrateur vous pouvez quand même la modifier</p>";
        echo "<form method=\"post\" action=\"actions/actionBenevoleComptage.php\">
                <input type=\"hidden\" name=\"ZoneSelectionner\" value=\"".$ZoneSelectionner."\">
                        <div class=\"d-flex justify-content-end\">
                            <button type=\"submit\" name=\"decloturer\" class=\"btn btn-primary\">
                                Décloturer
                            </button>
                        </div>
                    </form>";
    }
}

?>

<!--Fonction pour les deux boutons d'ajouts et retrait des espèces (compteurs +/-)-->
<script>
    var lat = 0;
    var lng = 0;
    champ = 0;

    function increment(nombre) {
        window.document.getElementById('nombre' + nombre).value++;
    }

    function decrement(nombre) {
        window.document.getElementById('nombre' + nombre).value--;
    }


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(callback, erreur);
    }

    function callback(position) {
        lat = position.coords.latitude;
        lng = position.coords.longitude;
        document.getElementById('mareeDefautLat').value = lat;
        document.getElementById('mareeDefautLon').value = lng;
        console.log(lat, lng);
    }

    function erreur(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                console.log('L\'utilisateur a refusé la demande');
                break;
            case error.POSITION_UNAVAILABLE:
                console.log('Position indéterminée');
                break;
            case error.TIMEOUT:
                console.log('Réponse trop lente');
                break;
        }
    }


</script>
<div class="mx-auto">'*' -> saisie obligatoire !</div>

<div class="row mt-3">
    <div class="mx-auto mt-3">
        <form class="needs-validation" method="post" action="selectionBenevoleComptage.php">
            <input type="hidden" class="form-control" id="ZoneSelectionner"
                   name="ZoneSelectionner" value="<?php echo $ZoneSelectionner ?>">
            <button type="submit" class="btn btn-outline-primary">Ajout Espece</button>
        </form>
    </div>
    <div class="mx-auto mt-3">
        <a href="#gps" type="button" class="btn btn-outline-primary">Ajout Coordonnée GPS</a>
    </div>
</div>

<div class="mx-auto mt-3">
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th class="text-center ">nom</th>
                <th class="text-center ">nb*</th>
                <th class="text-center ">+/-</th>
            </tr>
            <?php

            $request = $bd->prepare("select e.id_Espece,e.nom,ze.nombre from `espece` e 
            join `zone_espece` ze on e.id_Espece=ze.id_Espece 
            join `zone` z on ze.id_Zone=z.id_Zone
            join `etude_plage` ep on z.id_Etude_Plage=ep.id_Etude_Plage
            where z.id_Zone=:ZoneSelectionner");
            $request->bindParam(":ZoneSelectionner", $ZoneSelectionner);
            $request->execute();
            $lines = $request->fetchAll();
            $compteur = 0;
            ?>
            <form class="needs-validation" method="post" action="actions/actionBenevoleComptage.php">
                <?php foreach ($lines as $line) { ?>
                    <tr>
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="id_Espece<?php echo $compteur ?>"
                                   name="id_Espece<?php echo $compteur ?>" value="<?php echo $line["id_Espece"] ?>">
                        </div>
                        <td class="NoPaddingSide" id="espece">
                            <div class="form-group">
                                <input type="text" readonly class="form-control" id="nom<?php echo $compteur ?>"
                                       name="nom<?php echo $compteur ?>" value="<?php echo $line["nom"] ?>">
                            </div>
                        </td>
                        <td id="nb">
                            <div class="form-group">
                                <input type="number" class="form-control" id="nombre<?php echo $compteur ?>" required
                                       name="nombre<?php echo $compteur ?>" value="<?php echo $line["nombre"] ?>">
                            </div>
                        </td>
                        <td class="replacementTd">
                            <a id="plus" onclick="increment(<?php echo $compteur ?>);"
                               class="btn btn-danger replacementBouton"><i
                                        class="fas fa-angle-up"></i></a>
                            <a id="moins" onclick="decrement(<?php echo $compteur ?>);"
                               class="btn btn-danger replacementBouton"><i
                                        class="fas fa-angle-down"></i></a>
                        </td>
                    </tr>
                    <?php
                    $compteur++;
                } ?>
                <div class="form-group">
                    <input type="hidden" readonly class="form-control" id="compteur"
                           name="compteur" value="<?php echo $compteur ?>">
                </div>
                <div class="form-group">
                    <input type="hidden" readonly class="form-control" id="ZoneSelectionner"
                           name="ZoneSelectionner" value="<?php echo $ZoneSelectionner ?>">
                </div>
                <?php
                $request = $bd->prepare("select * from `zone` z where z.id_Zone=:ZoneSelectionner");
                $request->bindParam(":ZoneSelectionner", $ZoneSelectionner);
                $request->execute();
                $lines = $request->fetchAll();
                ?>
        </table>
    </div>
    <table class="table bg-secondary row">
        <div class="d-flex justify-content-end">
            <button type="submit" name="Save" class="btn btn-primary">
                Save
            </button>
        </div>
    </table>
    <table class="table bg-secondary">
        <div>Libelle</div>
        <tr>
            <th class="text-light">Libelle*</th>
        </tr>
        <td>
            <div class="form-group">
                <input type="text" class="form-control" id="libelle"
                       name="libelle" placeholder="ex: Zone 1" value="<?php echo $lines[0]["libelle"]; ?>">
            </div>
        </td>
    </table>
    <div class="d-flex justify-content-center my-5">
    <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $lines[0]["MareeHauteLatPos1"]?>,<?php echo $lines[0]["MareeHauteLonPos1"] ?>
    &size=1200x1000&maptype=roadmap
    &markers=color:blue%7Clabel:A%7C<?php echo $lines[0]["MareeHauteLatPos1"]?>,<?php echo $lines[0]["MareeHauteLonPos1"] ?>
    &markers=color:green%7Clabel:B%7C<?php echo $lines[0]["MareeHauteLatPos2"]?>,<?php echo $lines[0]["MareeHauteLonPos2"] ?>
    &markers=color:red%7Clabel:C%7C<?php echo $lines[0]["MareeBasseLatPos1"]?>,<?php echo $lines[0]["MareeBasseLonPos1"] ?>
    &markers=color:red%7Clabel:D%7C<?php echo $lines[0]["MareeBasseLatPos2"]?>,<?php echo $lines[0]["MareeBasseLonPos2"] ?>
    &zoom=17
    &key=AIzaSyD9oFdtfNI3g4dEwNAaB93zKTT5mSKZWm4" alt="">
    </div>
        <table class="table bg-primary">

        <div id="gps">Position GPS lors du chargement de la page</div>
        <tr>
            <th class="text-light">latitude</th>
            <th class="text-light">longitude</th>

        </tr>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeDefautLat"
                       name="mareeDefaut" placeholder="ex: 51.498134" pattern="^-?\d+([.,]\d+)?$"
                       value=""></div>

        </td>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeDefautLon"
                       name="mareeDefautLon" placeholder="ex: -0.201755" pattern="^-?\d+([.,]\d+)?$"
                       value="">
            </div>
        </td>
    </table>
    <div class="my-2 d-flex justify-content-end font-weight-bolder text-danger"><i
                class="fas fa-exclamation-triangle mr-2"></i>Pensées a sauvegardés vos données régulièrement !
    </div>
    <table class="table bg-secondary">
        <div id="gps">Marée Haute Pos A</div>
        <tr>
            <th class="text-light">latitude A</th>
            <th class="text-light">longitude A</th>
        </tr>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeHLat1"
                       name="mareeHLat1" placeholder="ex: 51.498134" pattern="^-?\d+([.,]\d+)?$"
                       value="<?php echo $lines[0]["MareeHauteLatPos1"]; ?>"></div>

        </td>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeHLon1"
                       name="mareeHLon1" placeholder="ex: -0.201755" pattern="^-?\d+([.,]\d+)?$"
                       value="<?php echo $lines[0]["MareeHauteLonPos1"]; ?>">
            </div>
        </td>
    </table>
    <table class="table bg-secondary">
        <div>Marée Haute Pos B</div>
        <tr>
            <th class="text-light">latitude B</th>
            <th class="text-light">longitude B</th>
        </tr>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeHLat2"
                       name="mareeHLat2" placeholder="ex: 51.498134" pattern="^-?\d+([.,]\d+)?$"
                       value="<?php echo $lines[0]["MareeHauteLatPos2"]; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeHLon2"
                       name="mareeHLon2" placeholder="ex: -0.201755" pattern="^-?\d+([.,]\d+)?$"
                       value="<?php echo $lines[0]["MareeHauteLonPos2"]; ?>">
            </div>
        </td>
    </table>
    <table class="table bg-secondary">
        <div>Marée basse Pos C</div>
        <tr>
            <th class="text-light">latitude C</th>
            <th class="text-light">longitude C</th>
        </tr>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeBLat1"
                       name="mareeBLat1" placeholder="ex: 51.498134" pattern="^-?\d+([.,]\d+)?$"
                       value="<?php echo $lines[0]["MareeBasseLatPos1"]; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeBLon1"
                       name="mareeBLon1" placeholder="ex: -0.201755" pattern="^-?\d+([.,]\d+)?$"
                       value="<?php echo $lines[0]["MareeBasseLonPos1"]; ?>">
            </div>
        </td>
    </table>
    <table class="table bg-secondary">
        <div>Marée basse Pos D</div>
        <tr>
            <th class="text-light">latitude D</th>
            <th class="text-light">longitude D</th>
        </tr>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeBLat2"
                       name="mareeBLat2" placeholder="ex: 51.498134" pattern="^-?\d+([.,]\d+)?$"
                       value="<?php echo $lines[0]["MareeBasseLatPos2"]; ?>">
            </div>
        </td>
        <td>
            <div class="form-group">
                <input class="form-control" id="mareeBLon2"
                       name="mareeBLon2" placeholder="ex: -0.201755" pattern="^-?\d+([.,]\d+)?$"
                       value="<?php echo $lines[0]["MareeBasseLonPos2"]; ?>">
            </div>
        </td>
    </table>
    <table class="table bg-secondary row">
        <div class="d-flex justify-content-end">
            <button type="submit" name="Save" class="btn btn-primary">
                Save
            </button>
        </div>
    </table>
    <table class="table bg-secondary row">
        <div class="d-flex justify-content-end">
            <button type="submit" name="Clore" class="btn btn-primary" onclick="return confirm('Etes vous sûr ?')">
                Clore la zone !
            </button>
        </div>
    </table>
</div>

<?php
require "footer.php";
mon_footer();
?>
