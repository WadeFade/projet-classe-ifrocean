<?php
session_start();
include "header.php";
mon_header("Page Admin Plage");
require "config.php";

$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$EtudePlageSelectionner = filter_input(INPUT_POST,"EPs");
if ($EtudePlageSelectionner==null){
$EtudePlageSelectionner = filter_input(INPUT_GET, "EP");
}

if ($EtudePlageSelectionner == null) {
    $EtudeSelectionner = filter_input(INPUT_POST, "EtudeSelectionner");
    if ($EtudeSelectionner == null) {
        $EtudeSelectionner = filter_input(INPUT_GET, "EtudeSelection");
        if($EtudeSelectionner==null){
            $EtudeSelectionner=1;
        }
    }
    $PlageSelectionner = filter_input(INPUT_POST, "PlageSelectionner");
    if ($PlageSelectionner == null) {
        $PlageSelectionner = filter_input(INPUT_GET, "PlageSelection");
        if ($PlageSelectionner==null){
            $PlageSelectionner=1;
        }
    }

} else{
    $request = $bd->prepare("select * from `etude_plage` ep where id_Etude_Plage=:idEtudePlage");
    $request->bindParam(":idEtudePlage",$EtudePlageSelectionner);
    $request->execute();
    $lines=$request->fetchAll();
    $PlageSelectionner=$lines[0]["id_Plage"];
    $EtudeSelectionner=$lines[0]["id_Etude"];
}

$id_Compte = $_SESSION["id_Compte"];
$request = $bd->prepare("select * from `plage` p where p.id_Plage=:PlageSelectionner");
$request->bindParam(":PlageSelectionner", $PlageSelectionner);
$request->execute();
$lines = $request->fetchAll();

?>
    <div class="mx-auto">'*' -> saisie obligatoire !</div>
    <div class="mx-auto mt-3">
        <div class="d-flex justify-content-end mb-3">
            <a href="pageBenevole.php" class="btn btn-outline-primary">Création d'une nouvelle zone</a>
        </div>
        <div class="d-flex justify-content-end mb-3">
            <a href="PageAdminPlageSelectionAutrePlage.php" class="btn btn-outline-primary">Changement de la plage séléctionner</a>
        </div>
        <table class="table bg-secondary">
            <th class="text-light">Plage n°<?php echo $PlageSelectionner ?>
                <div class="d-flex justify-content-end">
                    <form class="needs-validation" method="post" action="createPlage.php">
                        <button type="submit" name="ajout" class="btn btn-primary"
                                onclick="return confirm('Etes vous sûr ?')">
                            <i class="fas fa-plus-square"></i> Nouvelle Plage
                        </button>
                    </form>
                </div>
            </th>
        </table>
        <form class="needs-validation" method="post" action="actions/actionSavePageAdminPlage.php">
            <input type="hidden" name="PlageSelectionner" value="<?php echo $PlageSelectionner ?>">
            <input type="hidden" name="EtudeSelectionner" value="<?php echo $EtudeSelectionner ?>">
            <table class="table bg-secondary">
                <tr>
                    <th class="text-light">Nom de la plage*</th>
                </tr>
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" id="nom"
                               name="nom" placeholder="ex: Plage des tontons nageurs"
                               value="<?php echo $lines[0]["nom"]; ?>">
                    </div>
                </td>
            </table>
            <table class="table bg-secondary">
                <tr>
                    <th class="text-light">Département*</th>
                </tr>
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" id="departement"
                               name="departement" placeholder="ex: Loire-atlantique"
                               value="<?php echo $lines[0]["departement"]; ?>">
                    </div>
                </td>
            </table>
            <table class="table bg-secondary">
                <tr>
                    <th class="text-light">Commune*</th>
                </tr>
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" id="commune"
                               name="commune" placeholder="ex: Nantes"
                               value="<?php echo $lines[0]["commune"]; ?>">
                    </div>
                </td>
            </table>
            <table class="table bg-secondary row">
                <div class="d-flex justify-content-end">
                    <button type="submit" name="Save" class="btn btn-primary">
                        Save Plage
                    </button>
                </div>
            </table>
            <?php
            $request = $bd->prepare("select e.id_Etude,p.id_Plage,ep.id_Etude_Plage,ep.PlageSuperficieTotale,e.titre from `plage` p 
    join `etude_plage` ep on p.id_Plage = ep.id_Plage
join `etude` e on ep.id_Etude = e.id_Etude where p.id_Plage=:PlageSelectionner");
            $request->bindParam(":PlageSelectionner", $PlageSelectionner);
            $request->execute();
            $lignes = $request->fetchAll();
            //        début boucle
            $compteur = 0;
            foreach ($lignes as $value) {
                ?>
                <table class="table bg-secondary">
                    <tr>
                        <th class="text-light">Superficie Plage [<?php echo $lines[0]["nom"]; ?>]
                            dans l'étude n°<?php echo $value["id_Etude"] ?> [<?php echo $value["titre"] ?>]
                        </th>
                    </tr>
                    <td>
                        <div class="form-group">
                            <input type="number" class="form-control" id="superficie<?php echo $compteur ?>"
                                   name="superficie<?php echo $compteur ?>" placeholder="ex : 120"
                                   value="<?php echo $value["PlageSuperficieTotale"]; ?>">
                        </div>
                    </td>
                    <input type="hidden" name="EtudePlageSelectionner<?php echo $compteur ?>"
                           value="<?php echo $value["id_Etude_Plage"] ?>">
                </table>
                <?php
                $compteur++;
            }
            //      fin boucle
            ?>
            <input type="hidden" name="compteur" value="<?php echo $compteur ?>">
            <table class="table bg-secondary row">
                <div class="d-flex justify-content-end">
                    <button type="submit" name="SaveSuperficie" class="btn btn-primary">
                        Save Plage et Superficie
                    </button>
                </div>
            </table>
        </form>
    </div>


<?php
$request = $bd->prepare("select e.id_Etude,p.id_Plage,ep.id_Etude_Plage,ep.PlageSuperficieTotale,e.titre from `plage` p join `etude_plage` ep on p.id_Plage = ep.id_Plage
join `etude` e on ep.id_Etude = e.id_Etude where p.id_Plage=:PlageSelectionner");
$request->bindParam(":PlageSelectionner", $PlageSelectionner);
$request->execute();
$lignes = $request->fetchAll();
//        début boucle
$acompteur = 0;
foreach ($lignes as $value) {
    ?>
    <table class="table bg-secondary">
        <tr>
            <th class="text-light"><?php echo $lines[0]["nom"]; ?>
                dans l'étude n°<?php echo $value["id_Etude"] ?> [<?php echo $value["titre"] ?>] -
                id[<?php echo $value["id_Etude_Plage"] ?>]
            </th>
        </tr>
    </table>
    <?php
    $request = $bd->prepare("select * from zone z join etude_plage ep on z.id_Etude_Plage = ep.id_Etude_Plage
where ep.id_Etude_Plage=:EtudePlageSelectionner");
    $request->bindParam(":EtudePlageSelectionner", $value["id_Etude_Plage"]);
    $request->execute();
    $val = $request->fetchAll();
//        début boucle
    $compteur = 0;
    foreach ($val as $valeur) {
        ?>
        <div class="d-flex justify-content-end align-items-center">
            <table class="table bg-secondary col-10 ">
                <tr>
                    <th class="text-light align-">zone n° [<?php echo $valeur["id_Zone"]; ?>]
                        <div class="d-flex justify-content-end">
                            <div class="mx-2">
                                <a href="benevoleComptage.php?PZ=<?php echo $valeur["id_Zone"]; ?>"
                                   class="btn btn-info"><i class="fas fa-pen"></i></a>
                            </div>
                            <div class="mx-2">
                                <a onclick="return confirm('Etes vous sûr de bien vouloir supprimé cette zone appartenant a cette plage ?')"
                                <a href="actions/actionSuppressionPageAdminPlage.php?id_Etude_Plage=<?php echo $value["id_Etude_Plage"] ?> ?>&amp;ZoneSelectionner=<?php echo $valeur["id_Zone"]; ?>"
                                   class="btn btn-danger"><i class="fas fa-times"></i></a>
                            </div>
                        </div>
                    </th>
                </tr>
            </table>
        </div>
        <?php
        $compteur++;
    }
}
//      fin boucle
?>

    <div class="d-flex justify-content-end mb-3">
        <a href="pageAdmin.php" class="btn btn-outline-primary">Retour</a>
    </div>
    </table>
    </div>


<?php
include "footer.php";
mon_footer();
?>