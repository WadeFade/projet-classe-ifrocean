<?php
session_start();
require "headerAll.php";
mon_header("Page Admin Etude");
require "config.php";

if (isset($_SESSION["username"])){
    if ($_SESSION["estAdmin"] != 1) {
        echo "<h2>Vous n'avez pas accès à cette page !</h2>";
        require_once "footer.php";
        mon_footer();
        die();
    }
} else {
    echo "<h2>Vous devez vous identifier !</h2>";
    require_once "footer.php";
    mon_footer();
    die();
}
$id_Compte = $_SESSION['id_Compte'];


// fonction etude Close -> renvois une chaine qui indique si elle est terminée ou non
function etudeClose($EtudeSelectionner)
{
    $bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
    $requ = $bd->prepare("select dateFin from `etude` e where e.id_Etude=:EtudeSelectionner");
    $requ->bindParam(":EtudeSelectionner", $EtudeSelectionner);
    $requ->execute();
    $line = $requ->fetchAll();
    if ($line[0]['dateFin'] == '0000-00-00'){
        $line[0]['dateFin'] = NULL;
    }
    if ($line[0]["dateFin"] != NULL) {
        return "<table class=\"table bg-warning\"><th class=\"text-light\">L'étude est terminée !</th></table>";
    } else {
        return "<table class=\"table bg-info\"><th class=\"text-light\">L'étude est en cours !</th></table>";
    }
}

// fonction proprioEtude , renvoie l'id du compte lié a l'étude, si aucun renvois l'id du compte admin utilisé a ce moment
function proprioEtude($id_CompteConnecter, $id_CompteEtude)
{
    if ($id_CompteEtude == null) {
        return $id_CompteConnecter;
    } else {
        return $id_CompteEtude;
    }
}

$EtudeSelectionner = filter_input(INPUT_POST, "EtudeSelectionner");
if ($EtudeSelectionner == null) {
    $EtudeSelectionner = filter_input(INPUT_GET, "EtudeSelection");
}

$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$request = $bd->prepare("select * from `etude` e where e.id_Etude=:EtudeSelectionner");
$request->bindParam(":EtudeSelectionner", $EtudeSelectionner);
$request->execute();
$lines = $request->fetchAll();

?>
<div class="mx-auto">'*' -> saisie obligatoire !</div>
<div class="mx-auto mt-3">
    <div class="d-flex justify-content-end mb-3">
        <a href="PageAdminEtudeSelectionAutreEtude.php" class="btn btn-outline-primary">Changement de l'étude séléctionner</a>
    </div>
    <table class="table bg-secondary">
        <th class="text-light">Etude n°<?php echo $EtudeSelectionner ?>
            <div class="d-flex justify-content-end">
<!--                <form class="needs-validation" method="post" action="actions/actionAjoutPageAdminEtude.php">-->
<!--                    <button type="submit" name="ajout" class="btn btn-primary"-->
<!--                            onclick="return confirm('Etes vous sûr ?')">-->
<!--                        <i class="fas fa-plus-square"></i> Nouvelle étude-->
<!--                    </button>-->
<!--                </form>-->
            </div>
        </th>
    </table>
    <form class="needs-validation" method="post" action="actions/actionModificationPageAdminEtude.php">
        <input type="hidden" name="EtudeSelectionner" value="<?php echo $EtudeSelectionner ?>">
        <input type="hidden" id="id_Compte" name="id_Compte"
               value="<?php echo proprioEtude($id_Compte, $lines[0]["id_Compte"]) ?>">
        <?php echo etudeClose($EtudeSelectionner) ?>
        <table class="table bg-secondary">
            <tr>
                <th class="text-light">Titre de l'étude*</th>
            </tr>
            <td>
            <div class="form-group">
                <input type="text" class="form-control" id="Titre"
                       name="Titre" placeholder="ex: Etude côte ouest 2019" value="<?php echo $lines[0]["titre"]; ?>">
            </div>
            </td>
        </table>
        <table class="table bg-secondary">
            <tr>
                <th class="text-light">Description*</th>
            </tr>
            <td>
            <div class="form-group">
                <input type="text" class="form-control" id="Description"
                       name="Description" placeholder="ex: étude de la côte ouest 2019, destinée a ..."
                       value="<?php echo $lines[0]["description"]; ?>">
            </div>
            </td>
        </table>
        <table class="table bg-secondary">
            <tr>
                <th class="text-light">Date de début*</th>
            </tr>
            <td>
            <div class="form-group">
                <input type="date" class="form-control" id="DateDebut"
                       name="DateDebut" placeholder="ex: 25/07/2019"
                       value="<?php echo $lines[0]["dateDebut"]; ?>">
            </div>
            </td>
        </table>
        <table class="table bg-secondary">
            <tr>
                <th class="text-light">Date de fin</th>
            </tr>
            <td>
            <div class="form-group">
                <input type="date" class="form-control" id="DateFin"
                       name="DateFin" placeholder="ex: 17/12/2019"
                       value="<?php echo $lines[0]["dateFin"]; ?>">
            </div>
            </td>
        </table>
        <table class="table bg-secondary row">
            <div class="d-flex justify-content-end">
                <button type="submit" name="Save" class="btn btn-primary">
                    Save
                </button>
            </div>
        </table>
        <table class="table bg-secondary row">
            <div class="d-flex justify-content-end">
<!--                <button type="submit" name="Clore" class="btn btn-primary"-->
<!--                        onclick="return confirm('Etes vous sûr ? ATTENTION - ACTION DEFINITIVE')">-->
<!--                    Clore l'étude !-->
<!--                </button>-->
            </div>
        </table>
    </form>
</div>

<?php
$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$request = $bd->prepare("select * from `etude` e where e.id_Etude=:id_Etude");
$request->bindParam(":id_Etude", $id_Etude);
$request->execute();
$lines = $request->fetchAll();

$request = $bd->prepare("select p.id_Plage, p.nom from `plage` p");
$request->execute();
$lines = $request->fetchAll();
?>

<form action="actions/actionSelectionPageAdminEtude.php" method="post">
    <div class="form-group">
        <input type="hidden" name="EtudeSelectionner" value="<?php echo $EtudeSelectionner ?>">
        <input type="hidden" id="id_Plage" name="id_Plage" value="">
        <label for="sel1" class="align-bottom">Selection des plages :</label>
        <select class="form-control" id="selectPlage" name="selectPlage">
            <option value="" selected>Selectionner une Plage</option>
            <?php
            foreach ($lines as $line) { // début boucle
                ?>
                <option value="<?php echo $line["id_Plage"] ?>"><?php echo $line["nom"] ?></option>
                <?php // fin boucle
            }
            ?>
        </select>
        <div class="float-right my-3">
            <button id="btnSend" disabled type="submit" class="btn btn-primary"><i class="fas fa-plus-square"></i>
                Ajout Plage existante
            </button>
        </div>
    </div>
</form>
<form action="actions/actionSaveSuperficiePageAdminEtude.php" method="post">
    <table class="table bg-secondary">
        </br>
        <div>Plage selectionner dans l'étude N°[<?php echo $EtudeSelectionner ?>]</div>
        <tr>
            <th class="text-light">Id</th>
            <th class="text-light">Nom</th>
            <th class="text-light">Superficie totale</th>
        </tr>
        <?php
        $request = $bd->prepare("select ep.PlageSuperficieTotale,ep.id_Etude_Plage,p.nom,p.id_Plage from `etude_plage` ep join `plage` p on ep.id_Plage=p.id_Plage
        join `etude` e on e.id_Etude=ep.id_Etude where ep.id_Etude=:EtudeSelectionner");
        $request->bindParam(":EtudeSelectionner", $EtudeSelectionner);
        $request->execute();
        $lines = $request->fetchAll();
        $compteur=0;
        foreach ($lines as $line) { // début boucle
            ?>
            <tr>
                <input type="hidden" name="etudePlage<?php echo $compteur?>" value="<?php echo $line["id_Etude_Plage"] ?>">
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" id="id_Plage<?php echo $compteur?>"
                               readonly
                               name="id_Plage<?php echo $compteur?>"
                               value="<?php echo $line["id_Plage"] ?>">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="text" class="form-control" id="nom<?php echo $compteur?>" readonly
                               name="nom<?php echo $compteur?>" value="<?php echo $line["nom"] ?>">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" class="form-control" id="Superficie<?php echo $compteur?>"
                               name="Superficie<?php echo $compteur?>"
                               value="<?php echo $line["PlageSuperficieTotale"] ?>">
                    </div>
                </td>
                <td>
                    <div class="d-flex justify-content-end">
                        <div class="mx-2">
                            <a href="PageAdminPlage.php?PlageSelection=<?php echo $line["id_Plage"]; ?>"
                               class="btn btn-info"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="mx-2">
                            <a onclick="return confirm('Etes vous sûr de bien vouloir supprimer cette plage de cette étude ?')"
                               href="actions/actionSuppressionPageAdminEtude.php?id_Plage=<?php echo $line["id_Plage"] ?>&amp;EtudeSelectionner=<?php echo $EtudeSelectionner ?>"
                               class="btn btn-danger"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </td>
            </tr>
            <?php // fin boucle
            $compteur++;
        }
        ?>
        <input type="hidden" name="EtudeSelectionner" value="<?php echo $EtudeSelectionner ?>">
        <input type="hidden" name="compteur" value="<?php echo $compteur?>">
        <table class="table bg-secondary">
            <div class="d-flex justify-content-end">
                <button name="SavePlage" type="submit" class="btn btn-primary"><i class="fas fa-sync-alt"></i> Save Plage</button>
            </div>
        </table>
    </table>
</form>

<table class="table bg-secondary">
    <div class="d-flex justify-content-end">
        <a href="PageAdminPlage.php?EtudeSelection=<?php echo $EtudeSelectionner?>" class="btn btn-primary"><i class="fas fa-plus-square"></i> Création / Modification Plage</a>
    </div>
</table>
<div class="d-flex justify-content-end mb-3">
    <a href="pageAdmin.php" class="btn btn-outline-primary">Retour</a>
</div>
</table>
</div>

<script>
    $(document).ready(function () {
        $('#selectPlage').change(function () {
            if ($(this).val() != '') {
                $('#id_Plage').val($(this).val());
                $('#btnSend').removeAttr("disabled", "disabled");
            } else {
                $('#id_Plage').val('');
                $('#btnSend').attr("disabled", "disabled");
            }
        });
    });
</script>

<?php
include "footer.php";
mon_footer();
?>
