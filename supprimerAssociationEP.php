<?php require "headerAll.php";
mon_header("Supprimer une association");

session_start();

if ($_SESSION["estAdmin"] != 1) {
    echo "<h2>Vous n'avez pas accès à cette page !</h2>";
    require_once "footer.php";
    mon_footer();
    die();
}

$id = filter_input(INPUT_GET, "EP");

require "config.php";
$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("select id_Etude_Plage, etude_plage.id_Etude, etude_plage.id_Plage, titre, nom from `etude_plage` join `etude` on etude_plage.id_Etude=etude.id_Etude join `plage` on etude_plage.id_Plage=plage.id_Plage where id_Etude_Plage=:id_Etude_Plage and etude_plage.id_Etude=etude.id_Etude and etude_plage.id_Plage=plage.id_Plage");
$requete->bindParam(":id_Etude_Plage", $id);
$requete->execute();
$lignes = $requete->fetchAll();


if (count($lignes) != 1) {
    //renvoyer une erreur 404
    echo "Cet id n'existe pas...";
    http_response_code(404);
    include 'footer.php';
    mon_footer();
    die();
}
$titre = $lignes[0]['titre'];
$nom = $lignes[0]['nom'];
?>

<h1 class="text-center">Supprimer l'association entre la plage et l'étude</h1>
<form method="post" action="actions/actionSuppressionAssociationEP.php">
    <h2 class="text-center">Êtes-vous sur de vouloir supprimer cette association (Etude - Plage) : <?php echo "<i>" . "$titre" . " - " . "$nom" . "</i>" ?></h2>
    <h2 class="text-center">avec toutes ses zones ?</h2>
    <input type="hidden" name="id" value="<?php echo $id ?>">


    <a href="pageAdmin.php" class="btn btn-primary pull-left">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>
    <button type="submit" class="btn btn-danger">Confirmer</button>
</form>

<?php require "footer.php";
mon_footer();
?>

