<?php
session_start();
include "header.php";
mon_header("Page Admin Plage");
require "config.php";

$bd = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$id_Compte = $_SESSION["id_Compte"];

$EtudePlageSelectionner = filter_input(INPUT_POST,"EPs");
if ($EtudePlageSelectionner==null){
    $EtudePlageSelectionner = filter_input(INPUT_GET, "EP");
}

if ($EtudePlageSelectionner == null) {
    $EtudeSelectionner = filter_input(INPUT_POST, "EtudeSelectionner");
    if ($EtudeSelectionner == null) {
        $EtudeSelectionner = filter_input(INPUT_GET, "EtudeSelection");
    }
    $PlageSelectionner = filter_input(INPUT_POST, "PlageSelectionner");
    if ($PlageSelectionner == null) {
        $PlageSelectionner = filter_input(INPUT_GET, "PlageSelection");
    }

} else{
    $request = $bd->prepare("select * from `etude_plage` ep where id_Etude_Plage=:idEtudePlage");
    $request->bindParam(":idEtudePlage",$EtudePlageSelectionner);
    $request->execute();
    $lines=$request->fetchAll();
    $PlageSelectionner=$lines[0]["id_Plage"];
    $EtudeSelectionner=$lines[0]["id_Etude"];
}


$request = $bd->prepare("select * from `etude_plage` ep where ep.id_Plage=:PlageSelectionner");
$request->bindParam(":PlageSelectionner", $PlageSelectionner);
$request->execute();
$lines = $request->fetchAll();

$request = $bd->prepare("select * from `plage` p");
$request->bindParam(":PlageSelectionner", $PlageSelectionner);
$request->execute();
$lines = $request->fetchAll();

?>

    <table class="table bg-secondary">
    <form action="PageAdminPlage.php" method="post">
        <div class="form-group">
            <input type="hidden" name="EtudeSelectionner" value="<?php echo $EtudeSelectionner ?>">
            <input type="hidden" id="idPlage" name="idPlage" value="">
            <label for="sel1">Selection de la plage :</label>
            <select class="form-control" id="PlageSelectionner" name="PlageSelectionner">
                <option value="" selected>Selectionner une plage</option>
                <?php
                foreach ($lines as $line) { // début boucle
                    ?>
                    <option value="<?php echo $line["id_Plage"] ?>"><?php echo $line["nom"] ?></option>
                    <?php // fin boucle
                }
                ?>
            </select>
            <div class="float-right my-3">
                <button id="btnSend" disabled type="submit" class="btn btn-primary"><i class="fas fa-plus-square"></i>
                    Changement de la plage
                </button>
            </div>
        </div>
    </form>
    </table>

    <div class="d-flex justify-content-end mb-3">
        <a href="pageAdmin.php" class="btn btn-outline-primary">Retour</a>
    </div>

    <script>
        $(document).ready(function () {
            $('#PlageSelectionner').change(function () {
                if ($(this).val() != '') {
                    $('#idPlage').val($(this).val());
                    $('#btnSend').removeAttr("disabled", "disabled");
                } else {
                    $('#idPlage').val('');
                    $('#btnSend').attr("disabled", "disabled");
                }
            });
        });
    </script>

<?php
include "footer.php";
mon_footer();
?>