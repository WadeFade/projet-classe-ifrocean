<?php
require "headerAll.php";
session_start();
mon_header("Créer une étude");

if ($_SESSION["estAdmin"] != 1) {
    echo "<h2>Vous n'avez pas accès à cette page !</h2>";
    require_once "footer.php";
    mon_footer();
    die();
}

$id_Compte = $_SESSION["id_Compte"];
?>

<h1 class="text-center">Créer une Plage</h1>
<form action="actions/actionCreatePlage.php" method="post">
    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom..." maxlength="50" required>
    </div>
    <div class="form-group">
        <label for="departement">Departement</label>
        <input type="text" class="form-control" id="departement" name="departement" placeholder="Departement..." required>
    </div>
    <div class="form-group">
        <label for="commune">Commune</label>
        <input type="text" class="form-control" id="commune" name="commune" placeholder="Commune..." required>
    </div>
    <input type="hidden" id="id_Compte" name="id_Compte" value="<?php echo $id_Compte ?>">
    <a href="pageAdmin.php" class="btn btn-primary pull-left">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>
    <button type="submit" class="btn btn-primary"><i class="fal fa-check"></i> Créer</button>
</form>

<?php require "footer.php";
mon_footer();
?>
