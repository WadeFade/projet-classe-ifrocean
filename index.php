<?php include "header.php";
session_start();

mon_header("Page de Connexion");
$token=rand(0,2000000000);
$_SESSION["token"]=$token;
$erreur = filter_input(INPUT_GET, "erreur");

?>

<h1 class="titleCenter">Connexion</h1>
<?php if ($erreur==1){
    echo "<div class=\"alert alert-danger formConnection\" role=\"alert\">Mauvais identifiant ou mot de passe !</div>";
}
?>
<form method="post" action="actions/actionConnexion.php">
    <input type="hidden" id="token" name="token" value="<?php echo $token ?>">
    <div class="form-group formConnection">
        <label for="email">Adresse mail</label>
        <input type="text" name="email" class="form-control" id="email" aria-describedby="email" placeholder="Votre adresse mail">
        <small id="emailHelp" class="form-text text-muted"></small>
    </div>
    <div class="form-group formConnection">
        <label for="password">Mot de passe</label>
        <input type="password" name="password" class="form-control" id="password" placeholder="Votre mot de passe">
        <small><a href="changePassword.php">Mot de passe oublié ?</a>
        <br>
        Pour les bénévoles : demander à un Administrateur.
        </small>
    </div>
    <div class="form-group form-check formConnection">
        <input type="checkbox" name="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Se souvenir de moi</label>
    </div>
    <div class="formConnection">
    <small><a href="inscription.php">Se créer un compte</a></small><br>
    <button type="submit" name="submit" class="btn btn-primary formCenter">Connexion</button>
    </div>
</form>


<?php include "footer.php";
mon_footer();
?>
